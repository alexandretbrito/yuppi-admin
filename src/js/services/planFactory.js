(function(){
	'use strict';
	/**
	* planFactory
	* Facilita o caminho para os conteúdo na base de dados
	* Alexandre Brito 2016
	*/
	angular.module('app')
	.factory('planFactory', planFactory);

	function planFactory($firebaseObject, $rootScope, $firebaseArray, accessFactory){
		
	var planGet = {
		getEvts: getEvts,
		getSingleEvt: getSingleEvt,
		insertEvt: insertEvt,
		removeEvt: removeEvt,
		insertShop: insertShop,
		insertChecklist: insertChecklist,
		insertCrono: insertCrono,
		insertSup: insertSup,
		insertBudget: insertBudget,
		retrieveTodo: retrieveTodo,
		insertTodo: insertTodo,
		retrieveTech: retrieveTech,
		insertTtech: insertTtech,
		insertGuest: insertGuest,
		makeGroup: makeGroup,
		delGroup: delGroup,
		getGroup: getGroup,
		insertGallery: insertGallery,
		retrieveGallery: retrieveGallery,
		retrieveMeeting: retrieveMeeting,
		insertMeeting: insertMeeting,
		allGroups: allGroups
	};
	return planGet;

		function getEvts(uid) {
			// body...
			var reccon = accessFactory.pegaEventList(uid);
			if(reccon){
				return reccon;
			}			
		}

		function getSingleEvt(uid, evID) {
			var trans = accessFactory.pegaEvento(uid);
			var trans2 = trans.child(evID);
			trans2.on("value", function(snapshot){
				var geriEvt = snapshot.val();
				return geriEvt;
			});
		}

		function insertEvt(uid,evtId, obj) {
			console.log("insertEvt")
			var salvation = accessFactory.pegaEventList(uid);
			//var slot = salvation.child(uid);
			salvation.child(evtId).set(obj);
		}

		function removeEvt(uid,evtId) {
			console.log("insertEvt")
			var salvation = accessFactory.pegaEventList(uid);
			//var slot = salvation.child(uid);
			salvation.child(evtId).remove();
		}		

		function insertShop(uid, evtId, obj) {
			console.log("insertShop")
			var compration = accessFactory.pegaShopList(uid);
			compration.child(evtId).set(obj);
		}

		function insertChecklist(uid, evtId, obj) {
			console.log("insertShop")
			var compration = accessFactory.pegaChecklist(uid);
			compration.child(evtId).set(obj);
		}

		function insertCrono(uid, evtId, obj) {
			console.log("insertShop")
			var compration = accessFactory.pegaCronogramas(uid);
			compration.child(evtId).set(obj);
		}	

		function insertSup(uid, evtId, obj) {
			console.log("insertShop")
			var compration = accessFactory.pegaFornecedores(uid);
			compration.child(evtId).set(obj);
		}

		function insertBudget(uid, evtId, obj) {
			console.log("insertShop")
			var doletas = accessFactory.pegaBalanco(uid);
			doletas.child(evtId).set(obj);
		}										

		//////////  TO DO  ////////////////////

		function retrieveTodo(uid, evtId){
			var trans = accessFactory.pegaTodoList(uid);
			var traste = trans.child(evtId);
			traste.on("value", function(snapshot){
				var editedAcad = snapshot.val();
				return editedAcad;
			});
		}
		
		function insertTodo(uid, evtId, obj) {
			console.log("insertTodo")
			var salvation = accessFactory.pegaTodoList(uid);
			//var slot = salvation.child(uid);
			salvation.child(evtId).set(obj);
		}

		//////////  VISITA TECNICA  ////////////////////

		function retrieveTech(uid, evtId){
			var trans = accessFactory.pegaTechList(uid);
			var traste = trans.child(evtId);
			traste.on("value", function(snapshot){
				var editedAcad = snapshot.val();
				return editedAcad;
			});
		}
		
		function insertTtech(uid, evtId, obj) {
			console.log("insertTodo")
			var salvation = accessFactory.pegaTechList(uid);
			//var slot = salvation.child(uid);
			salvation.child(evtId).set(obj);
		}

		//////

		function insertGuest(uid, evtId, obj) {
			console.log("insertGuest")
			var salvation = accessFactory.pegaGuestList(uid);
			//var slot = salvation.child(uid);
			salvation.child(evtId).set(obj);
		}

		//////////  GRUPOS  ////////////////////

		function makeGroup(evtId, obj) {
			var agrupa = accessFactory.pegaGrouper(evtId);
			agrupa.set(obj);
		}

		function delGroup(evtId) {
			var desgrupa = accessFactory.pegaGrouper(evtId);
			desgrupa.remove();
		}

		function getGroup(evtId) {
		  var graups = accessFactory.pegaGrouper(evtId);
		  return graups;
		}

		function allGroups() {
			var grumps = accessFactory.daGroup()
			return grumps;
		}

		//////////  GALERIAS  ////////////////////

		function insertGallery(ownerID, obj) {
			var salvation = accessFactory.getGallery(ownerID);
			salvation.set(obj);
			$rootScope.$broadcast("galleryDone");
		}

		function retrieveGallery(ownerID) {
			var pegueition = accessFactory.getGallery(ownerID);
			return pegueition;
		}

		//////////  Reuniões  ////////////////////

		function retrieveMeeting(uid, evtId){
			var trans = accessFactory.getMeetings(uid);
			var traste = trans.child(evtId);
			traste.on("value", function(snapshot){
				var editedAcad = snapshot.val();
				return editedAcad;

			});
		}
		
		function insertMeeting(uid, evtId, obj) {
			console.log("insertTodo")
			var salvation = accessFactory.getMeetings(uid);
			//var slot = salvation.child(uid);
			salvation.child(evtId).set(obj);
		}

	}; // fim da função principal
})(); // fim do arquivo JS	