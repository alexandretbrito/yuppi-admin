(function() {
'use_strict';

angular.module('app').controller('ChronoKidCtrl', ChronoKidCtrl);

function ChronoKidCtrl($scope, $rootScope, planFactory, accessFactory, Utils, $filter, ipCookie, $timeout){

    console.log($rootScope.chooseEvt);
    $scope.editedID = $rootScope.chooseEvt;
    $scope.varUser = ipCookie('sessao');
    var chronObj = {} 

    $scope.listCheck = [];

    $scope.retroMes = true;
    $scope.indexed = 0;

    $scope.actualTab = "pre";
    $scope.restamMeses = 0;

    $scope.timeBefore = [];

    $scope.listPreKid = []; //lista de Pre Wedding
    $scope.listFestaKid = [];//Lista da Festa  

    $scope.tab = 1;

    $scope.setTab = function(newTab){
      $scope.tab = newTab;
      if(newTab == 1){
        $scope.actualTab = "pre";
      }if(newTab == 2){
        $scope.actualTab = "party";
      }
    };

    $scope.isSet = function(tabNum){
      console.log ($scope.actualTab) 
      return $scope.tab === tabNum;
    };


    $scope.starterKid = function(){
      console.log("starter");
      var trans = accessFactory.pegaCronogramas($scope.varUser.uid);
      var traste = trans.child($scope.editedID);
      traste.on("value", function(snapshot){
        chronObj = snapshot.val();
        console.log(chronObj)
        chron_passodois()
      });      
    }


    function chron_passodois() {
      // body...
      if($rootScope.asInvited){
         $timeout(function(){
             if(chronObj == undefined){
                console.log("não tem")
                $scope.firstContact();
            }
           else{
                console.log("tem")
                $scope.secondContact();
            }
         }, 1000);
      }else{
         $timeout(function(){
             if(chronObj == undefined){
                console.log("não tem")
                $scope.firstContact();
            }
           else{
                console.log("tem")
                $scope.secondContact();
            }
         }, 1000);        
      }

  };

        $scope.showTaskPrompt = function() {
          if($scope.actualTab === "pre"){
            var newShop = {
                item: '',
                time: 1,
                done: false
            };
          }else{
            var newShop = {
                item: '',
                time: "18:00",
                done: false
            };

              var chop = newShop.time;
              var arr = chop.split(':');
              var hr = arr[0];
              var mn = arr[1];
              var saveData = new Date(0);
              saveData.setHours(hr);
              saveData.setMinutes(mn);
              $scope.itemTime = {
                  value: saveData
              }; 
          }

            $scope.newShop = newShop;

            console.log($scope.itemTime);

            $scope.theBitsi = {};
            //$scope.hora = $scope.newShop.time;
            $scope.theBitsi.meses = $scope.newShop.time;
            $scope.theBitsi.values = $scope.timeBefore;
  
        };


          $scope.removeTaskNotSaved = function(){
            $scope.newShop = {};
          }

        $scope.saveTask = function() {
          console.log($scope.newShop)
          if($scope.newShop.item != ''){
                console.log("gravou")
                if($scope.actualTab === "pre"){
                  console.log($scope.theBitsi.meses +" meses faltantes")
                  $scope.newShop.time = $scope.theBitsi.meses;
                  $scope.listPreKid.push($scope.newShop);              
                }
                if($scope.actualTab === "party"){
                  var datafinal = $filter('date')($scope.itemTime.value, 'HH:mm');
                  $scope.newShop.time = datafinal;
                  console.log($scope.newShop)
                  $scope.listFestaKid.push($scope.newShop);
                }            
                $scope.insertIntoObj();
          }else{
              Utils.alertshow("Erro", "Indique o nome do item")
          }
                $('#newCronoWed').modal('hide')
        };

        $scope.cancelTask = function(indice) {
            //$scope.closeModal(indice);
        };

        $scope.firstContact = function(){
          console.log("------------------------------------------------------------")
          console.log("                      PRIMEIRO CONTATO                      ")
          console.log("------------------------------------------------------------")
          // Primeiro o PreWeeding
          var objPreKid = {};
          objPreKid = kidEntrega;
          //objPreKid = extraData.getPreWed();
          var objParty = {};
          objParty = kidFesta;
          //objParty = extraData.getParty();
          var all = {};
          all.pre = objPreKid;
          all.party = objParty;

          angular.forEach(all.pre, function(shops, key){
              $scope.listPreKid[key] = angular.copy(shops);
              console.log($scope.listPreKid)
          });
          //por último a festa
          angular.forEach(all.party, function(shops, key){
              shops.time = "18:00";
              console.log("---------------------------")
              console.log(shops)
              $scope.listFestaKid[key] = angular.copy(shops);
              $scope.listFestaKid[key].time = "18:00";
              
          });
          // gravar no $localStorage
          if($rootScope.asInvited){
            chronObj = all;
          }else{
            chronObj = all;
          }
          //gravar no Firebase        
          all = angular.copy(all)
          $scope.grave_me(all);
        };


        $scope.secondContact = function(){
          console.log("------------------------------------------------------------")
          console.log("                      SEGUNDO CONTATO                       ")
          console.log("------------------------------------------------------------")
          // Primeiro o PreWeeding
          var all = {};
          if($rootScope.asInvited){
            all = chronObj;
          }else{
            all = chronObj;
          }

          $scope.listPreKid = [];
          $scope.listFestaKid = [];

          angular.forEach(all.pre, function(shops, key){
              $scope.listPreKid[key] = angular.copy(shops);
              console.log($scope.listPreKid)
          });
          //por último a festa
          angular.forEach(all.party, function(shops, key){
              $scope.listFestaKid[key] = angular.copy(shops);
              console.log($scope.listFestaKid)
          });
          // gravar no $localStorage
          if($rootScope.asInvited){
            chronObj = all;            
          }else{
            chronObj = all;            
          }
          $scope.grave_me(all);
        }


  $scope.tabSelected = function(tab) {
    console.log(tab + ' Tab Selected');
    $scope.actualTab = tab;
    if($scope.actualTab === "pre"){

    }
    if($scope.actualTab === "party"){
          
    }
  };



///////////////////////////////////////////////////////////////////////////////////

    $scope.explainMe = function(state){
        state.done = true;
        if($rootScope.asInvited){
          chronObj = $scope.listCheck;          
        }else{
          chronObj = $scope.listCheck;          
        }
        $scope.insertIntoObj();
    }

    $scope.removeMe = function(itim){
        console.log(itim)
      if($scope.actualTab === "pre"){
          var aquele = $scope.listPreKid.indexOf(itim)
          console.log(aquele)
          $scope.listPreKid.splice(aquele, 1)
      }
      if($scope.actualTab === "party"){
          var aquele = $scope.listFestaKid.indexOf(itim)
          console.log(aquele)
          $scope.listFestaKid.splice(aquele, 1)          
      }
        $scope.insertIntoObj();
    };

    $scope.removefromUpdate = function(indice){
        //$scope.listCheck.splice(indi, 1)
    if($scope.actualTab === "pre"){
        $scope.listPreKid.splice($scope.indexed, 1)
    }
    if($scope.actualTab === "party"){
        $scope.listFestaKid.splice($scope.indexed, 1)          
    }
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    }    

    $scope.doneClicked = function(item){
        console.log($scope.actualTab);
        console.log($scope.actualTab)
        var indexed = 0;
        if($scope.actualTab === "pre"){
          indexed = $scope.listPreKid.indexOf(item);
          if($scope.listPreKid[indexed].done === false){
              $scope.listPreKid[indexed].done = true;
          }else{$scope.listPreKid[indexed].done = false}
        }
        if($scope.actualTab === "party"){
          indexed = $scope.listFestaKid.indexOf(item);
          if($scope.listFestaKid[indexed].done === false){
              $scope.listFestaKid[indexed].done = true;
          }else{$scope.listFestaKid[indexed].done = false}              
        }
        $scope.insertIntoObj();
    };

     $scope.updateClicked = function(){
        if($scope.modifItem.done === false){
            $scope.modifItem.done = true;
        }else{$scope.modifItem.done = false}
        //$scope.insertIntoObj();
    };   

    $scope.grave_me = function(objeto) {
            if($rootScope.asInvited){
              planFactory.insertCrono($scope.varUser.uid, $scope.editedID, objeto)
            }else{
              planFactory.insertCrono($scope.varUser.uid, $scope.editedID, objeto)
            }

    }

    $scope.gravacao = function(objeto){
            if($rootScope.asInvited){
              planFactory.insertCrono($scope.varUser.uid, $scope.editedID, objeto)
            }else{
              planFactory.insertCrono($scope.varUser.uid, $scope.editedID, objeto)
            }
    }


    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        if($scope.actualTab === "pre"){
          angular.forEach($scope.listPreKid, function(shopes, keyo){
              bje[keyo] = $scope.listPreKid[keyo];
              bje = angular.copy(bje);
              console.log(bje)
          });
          if($rootScope.asInvited){
            chronObj.pre = bje;
          }else{
            chronObj.pre = bje;
          }
        }
        if($scope.actualTab === "party"){
          angular.forEach($scope.listFestaKid, function(shopes, keyo){
              bje[keyo] = $scope.listFestaKid[keyo];
              bje = angular.copy(bje);
              console.log(bje)
          });
          if($rootScope.asInvited){
            chronObj.party = bje;               
          }else{
            chronObj.party = bje;               
          }
        }
        var prox = {};
        if($rootScope.asInvited){
          prox = chronObj;
        }else{
          prox = chronObj;
        }
        $scope.grave_me(prox);        
    };


    $scope.editTask = function(obito){
        //ionicToast.show('Este botão edita o item de compra', 'middle', false, 3000);
        $scope.modifItem = {};
        $scope.theItsi = {};
        $scope.modifItem = obito;
        $scope.theItsi.meses = $scope.modifItem.time;
        $scope.theItsi.values = $scope.timeBefore;
        console.log("entrou")
        console.log($scope.modifItem)

        if($scope.actualTab === "pre"){
          $scope.indexed = $scope.listPreKid.indexOf(obito);
          $scope.indiceMod = $scope.indexed;
        }
        if($scope.actualTab === "party"){
           $scope.retroMes = false;
           $scope.indexed = $scope.listFestaKid.indexOf(obito);
           $scope.indiceMod = $scope.indexed;
           if($scope.modifItem.time == 0){
                  var datainicial = new Date(0);
                  $scope.itemTime = {
                   value: datainicial
                  };  
              }else{
                  var chop = $scope.modifItem.time;
                  var arr = chop.split(':');
                  var hr = arr[0];
                  var mn = arr[1];
                  var datainiciada = new Date(0);
                  datainiciada.setHours(hr);
                  datainiciada.setMinutes(mn);
                  $scope.itemTime = {
                      value: datainiciada
                  };          
              }; 
        }
    };

 $scope.changeEventType = function(sel){
    console.log("changeEventType")
    console.log(sel);
    $scope.restamMeses = sel;
  }      

    $scope.updateTask = function() {
        console.log("saiu")
          if($scope.actualTab === "pre"){
            $scope.modifItem.time = $scope.restamMeses;
            $scope.listPreKid[$scope.indexed] = $scope.modifItem;
          }
          if($scope.actualTab === "party"){
            console.log($scope.itemTime.value)
            var datafinal = $filter('date')($scope.itemTime.value, 'HH:mm');
            $scope.modifItem.time = datafinal;
            console.log($scope.modifItem)
            $scope.listFestaKid[$scope.indexed] = $scope.modifItem;
            $scope.listFestaKid[$scope.indexed] = $scope.modifItem;               
          }
        $('#modCronoWed').modal('hide')
        $scope.insertIntoObj();
    };

    $scope.notUpdateTask = function(item){
        console.log("saiu sem salvar");
        $scope.modifItem.item = $scope.savedItem;
        $scope.modifItem.done = $scope.savedStatus;
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        console.log($scope.backupItem);
        console.log($scope.modifItem);
        delete $scope.backupItem;
        delete $scope.modifItem;
    }
  
  $scope.medetempo = function(hiato){
    var tempoFaltante = '';
    if(hiato == 0.25){
      tempoFaltante = "uma semana antes";
    }
    else if(hiato == 0.5){
     tempoFaltante = "duas semanas antes"; 
    }
    else if(hiato == 0.75){
     tempoFaltante = "três semanas antes"; 
    }
    else if(hiato == 0){
     tempoFaltante = "não definido"; 
    }
    else if(hiato == 1){
     tempoFaltante = "um mês antes"; 
    }
    else{
     tempoFaltante = hiato+" meses antes"; 
    }

    return tempoFaltante;
  }




    // Festa Infantil
    var kidEntrega = {
      0:{
        item: 'Móveis',
        time: .25,
        done: false
      },
      1:{
        item: 'Bolo',
        time: .25,
        done: false
      },
      2:{
        item: 'Doces',
        time: .25,
        done: false
      },
      3:{
        item: 'Lembranças',
        time: .25,
        done: false
      }
    };

    var kidFesta = {
      0:{
        item: 'Chegada do Bolo',
            time: "18:00",
        done: false
      },
      1:{
        item: 'Chegada dos Doces',
            time: "18:00",
        done: false
      },
      2:{
        item: 'Chegada das Lembranças',
            time: "18:00",
        done: false
      },
      3:{
        item: 'Início da festa',
            time: "18:00",
        done: false
      },
      4:{
        item: 'Animação/Recreação',
            time: "18:00",
        done: false
      },
      5:{
        item: 'Personagem Vivo',
            time: "18:00",
        done: false
      },
      6:{
        item: 'Parabéns',
            time: "18:00",
        done: false
      },
      7:{
        item: 'Encerramento',
            time: "18:00",
        done: false
      },
      8:{
        item: 'Desmontagem',
            time: "18:00",
        done: false
      }   
    };


    var antecipacao = [
      {tempo: 0, label:"não definido"},
      {tempo: .25, label:"1 semana antes"},
      {tempo: .50, label:"2 semanas antes"},
      {tempo: .75, label:"3 semanas antes"},   
      {tempo: 1, label:"1 mês antes"},
      {tempo: 2, label:"2 meses antes"},
      {tempo: 3, label:"3 meses antes"},
      {tempo: 4, label:"4 meses antes"},
      {tempo: 5, label:"5 meses antes"},
      {tempo: 6, label:"6 meses antes"},
      {tempo: 7, label:"7 meses antes"},
      {tempo: 8, label:"8 meses antes"},
      {tempo: 9, label:"9 meses antes"},
      {tempo: 10, label:"10 meses antes"},
      {tempo: 11, label:"11 meses antes"},
      {tempo: 12, label:"12 meses antes"},
      {tempo: 13, label:"13 meses antes"},
      {tempo: 14, label:"14 meses antes"},
      {tempo: 15, label:"15 meses antes"},
      {tempo: 16, label:"16 meses antes"},
      {tempo: 17, label:"17 meses antes"},
      {tempo: 18, label:"18 meses antes"}
    ];

      //$scope.timeBefore = extraData.getTimeEarly();
      $scope.timeBefore = antecipacao;




}; // fim da função
})(); // fim do documento