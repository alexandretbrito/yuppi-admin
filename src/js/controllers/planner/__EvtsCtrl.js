(function() {
'use_strict';

angular.module('app').controller('EvtsCtrl', EvtsCtrl);

function EvtsCtrl($scope, $rootScope, $location, $sessionStorage, Utils, $timeout, planFactory, accessFactory){

    $scope.tuttiEventi = [];
    $scope.timePaster = false;
	$scope.temEventos = false;
	var eventolandia = {};
	$scope.calendar = {}

	$scope.timePaster = false;

	$scope.usuarioAtivo = $sessionStorage.usuario;	
	console.log($scope.usuarioAtivo)
	console.log("estou em eventos")

	showContent()

	function starter() {
		// body...
		$rootScope.onEvent = false;
		var evPath = accessFactory.pegaEventList($scope.usuarioAtivo.uid);
			eventolandia = {};
			evPath.once('value').then(function(snapshot){
			eventolandia = snapshot.val();
			evalEvents();
			$scope.sharedEvts();

		});
	}


	$scope.$on("$destroy", function(event, data){
			$scope.timePaster = false;
			console.log("the events are leaving the building...")		
	})	

		$scope.newEvt = {};
		$rootScope.allEvents = {};	


////////////////////////////////////////////
function bringEvents(){
	console.log("bring events")
	console.log(eventolandia)
	var i=0
	var bjec = {};
	var events = [];
	bjec = eventolandia;
	//Utils.hide();
	angular.forEach(bjec, function(shops, key){
		var objent = {}
		objent.tipo = "evento";
		objent.chamamento = "Evento";
		objent.title = shops.name;
		objent.allDay = true;
		//
		var evtdate = shops.evtDay
		var brokeDate = [];
		brokeDate = evtdate.split("/");
		//
		objent.startTime = new Date(Date.UTC(brokeDate[2], brokeDate[1]-1, brokeDate[0], 3, 0, 0));
		objent.endTime = new Date(Date.UTC(brokeDate[2], brokeDate[1]-1, brokeDate[0], 3, 0, 0));
		objent.ID = shops.ID;
		objent.evtHour = shops.evtHour;
		objent.compareTime = datetoalign(brokeDate, objent.evtHour);
		events[i] = objent;
		//
			//datetoalign(brokeDate, objent.evtHour);
		//
	i++;
	});
	$rootScope.noMeetings = true;
	//$rootScope.indicator = "Eventos em ";
	$rootScope.labelDont = "Sem Eventos";
	return events;
}

function bringMeetings(){
	console.log("bring meetings")
	console.log(eventolandia)
	var i=0
	var bjec = {};
	var meetings = [];
	var meets = {};
	bjec = eventolandia;
	//Utils.hide();
	angular.forEach(bjec, function(shops, key){
		if(shops.reunioes){
			var meets = {};
			meets = shops.reunioes;
			angular.forEach(meets, function(conv, key2){
		 if(conv.done == false){
				var objent = {};
				console.log("tem reunião em " + conv.dia);
				objent.tipo = "reuniao";
				objent.chamamento = "Reuniâo";
				objent.title = conv.local;
				objent.allDay = true;
				var evtdate = conv.dia;
				var brokeDate = [];
				brokeDate = evtdate.split("/")
				//
				objent.startTime = new Date(Date.UTC(brokeDate[2], brokeDate[1]-1, brokeDate[0], 3, 0, 0));
				objent.endTime = new Date(Date.UTC(brokeDate[2], brokeDate[1]-1, brokeDate[0], 3, 0, 0));
				objent.ID = shops.ID;
				objent.evtHour = conv.hora;
				meetings.push(objent);
		 }
			});
		}
	});
	$rootScope.noMeetings = false;
	$rootScope.labelDont = "Sem Reuniões";
	return meetings;
};


////////////////////////////////////////////////////////////////////

$scope.sharedEvts = function(){
	var meetArr = [];
	var evtArr = [];
	var tuttiArrs = [];
	var evtArr = bringEvents();
	var meetArr = bringMeetings();
	tuttiArrs = evtArr.concat(meetArr);
	$scope.calendar.eventSource = tuttiArrs;
}


function evalEvents() {
	console.log("evalEvents")
		var i=0
		var bjec = {};
        var agora = new Date().getTime();
        $scope.tuttiEventi = [];
        bjec = eventolandia;
        angular.forEach(bjec, function(shops, key){
			$scope.tuttiEventi[i] = angular.copy(shops);
			$scope.tuttiEventi[i].compareDate = datetoalign($scope.tuttiEventi[i].evtDay, $scope.tuttiEventi[i].evtHour);
			$scope.tuttiEventi[i].past = datetofade($scope.tuttiEventi[i].compareDate, agora);
            i++;
        });
        console.log("vai")
        console.log($scope.tuttiEventi);
        $scope.$apply();
        //$scope.calendar.eventSource = bringEvents();
        //console.log($scope.calendar.eventSource);
}


	$scope.dia = function(evento){
		console.log("chegou o dia")
		var dengo = new Date(evento.evtDay);
		return dengo;
	}

	function datetoalign(evtdate, hora){
		var dateArr = [];
		dateArr = evtdate.split("/");
		var supertime = dateArr[2]+"-"+dateArr[1]+"-"+dateArr[0]+"T"+hora+":00";
		var numTime = new Date(supertime).getTime();
		return numTime;
	}

	function datetofade(datecomp, datenow){
		var bool_state;
		if(datecomp > datenow){
			bool_state = true;
		}else{
			bool_state = false;
		}
		return bool_state;
	}

	$scope.clicked = function(eventissimo){
		var daVez = {};
		daVez = eventissimo;
		console.log(daVez);
		$rootScope.globalEvtName = eventissimo.name;
		$rootScope.chooseEvt = eventissimo.ID;
		$rootScope.asInvited = false;
		$rootScope.onEvent = true;
		$location.path('eventos/eventounico');
	};

	$scope.delEvento = function(evento){
		var daVez = {};
		daVez = evento;

	};

	$scope.mandapraNovo = function(evento){
		$location.path('/eventos/novoevento');
	};

	$scope.deletaEvento = function(eventer){
		console.log(eventer)
		//delete $localStorage.eventos[eventer.uid];
		planFactory.removeEvt($scope.usuarioAtivo.uid , eventer.ID);
		starter();
	}

	$scope.clickToView = function(eventissimo){
		var daVez = {};
		daVez = eventissimo;
		console.log(daVez);
		$rootScope.eventoDaVez = daVez;
 		$scope.evtInfo = $rootScope.eventoDaVez;
		//$scope.openModal();
	};

   $scope.goneFly = function(evita){
      console.log("Gonna fly now!");
      $rootScope.chooseEvt = evita.ID
      console.log($rootScope.chooseEvt);
      $location.path("/eventos/editar_evento")
   }


   function showContent() {
   	// body...
	   $timeout(function(){
			$scope.timePaster = true;
			console.log("demorou...")
			starter();
	   }, 1200);
   }

	$scope.testClick= function(){
		alert("Acertou, miserávi!!!")
	}

/*	function bringEvents(){
			console.log("bring events")
			console.log(eventolandia)
			var i=0
	        var bjec = {};
			var events = [];
	        bjec = eventolandia;
	        angular.forEach(bjec, function(shops, key){
	        	var objent = {}
	        	objent.title = shops.name;
	        	objent.allDay = true;
	        	//
	        	var evtdate = shops.evtDay
	        	var brokeDate = [];
	        	brokeDate = evtdate.split("/")
	        	//
	        	objent.startTime = new Date(Date.UTC(brokeDate[2], brokeDate[1]-1, brokeDate[0], 3, 0, 0));
	        	objent.endTime = new Date(Date.UTC(brokeDate[2], brokeDate[1]-1, brokeDate[0], 3, 0, 0));
	        	objent.ID = shops.ID;
	        	objent.evtHour = shops.evtHour;
	        	//console.log(objent.startTime)
	        	events[i] = objent;
            i++;
	        });
	        return events;
	} */


	$scope.seeEvt = function(){
		$scope.calendar.eventSource = [];	
	   $scope.calendar.eventSource = bringEvents();
	   console.log("carregando eventos...")
	   console.log($scope.calendar.eventSource)
	}

	$scope.seeMeet = function(){
		$scope.calendar.eventSource = [];
	   $scope.calendar.eventSource = bringMeetings();
	   console.log("carregando reuniões...")
	   console.log($scope.calendar.eventSource)			
	}

	$scope.onViewTitleChanged = function (title) {
		$scope.viewTitle = title;
	};

	function datetoalign(dateArr, hora){
		console.log("aqui começa a zoeira da ordenação");
		var supertime = dateArr[2]+"-"+dateArr[1]+"-"+dateArr[0]+"T"+hora+":00";
		var numTime = new Date(supertime).getTime();
		console.log(new Date(numTime))
		return numTime;
	}

	$scope.calendar = {};
	$scope.changeMode = function (mode) {
		$scope.calendar.mode = mode;
	};

	$scope.today = function () {
		$scope.calendar.currentDate = new Date();
	};

	$scope.isToday = function () {
		var today = new Date(),
			currentCalendarDate = new Date($scope.calendar.currentDate);

		today.setHours(0, 0, 0, 0);
		currentCalendarDate.setHours(0, 0, 0, 0);
		return today.getTime() === currentCalendarDate.getTime();
	};	

	$scope.onEventSelected = function (event) {
		console.log()
		$rootScope.eventoDaVez = $rootScope.allEvents[event.ID]
		console.log($rootScope.eventoDaVez)
		$location.path('app/eventounico');
		$rootScope.asInvited = false;
	};

	$scope.onTimeSelected = function (selectedTime, events, disabled) {
		console.log('Selected time: ' + selectedTime + ', hasEvents: ' + (events !== undefined && events.length !== 0) + ', disabled: ' + disabled);

		$timeout(function(){	
		if(!events){
			console.log("Não tem Eventos. Coloco um?")
		}else{
			console.log(events[0].tipo)
			if (events[0].tipo == "reuniao") {
				$rootScope.indicator = "Reunião agendada em ";
			} else {
				$rootScope.indicator = "Evento em ";
			}
		}
		}, 600);
	};

	$scope.loadEvents = function () {
		$scope.seeEvt();
	};

}; // fim da função
})(); // fim do documento