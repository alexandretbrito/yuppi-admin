(function() {
'use_strict';

angular.module('app').controller('EditChecklistCtrl', EditChecklistCtrl);

function EditChecklistCtrl($scope, $rootScope, ionicToast, $ionicModal, $localStorage, $cordovaNetwork, planFactory, Utils, $ionicPopup){
    $scope.$parent.showHeader();
    console.log($rootScope.eventoDaVez);
    $scope.nome = $rootScope.eventoDaVez.name;
    $scope.theEvent = $rootScope.eventoDaVez.ID;   

    $scope.listCheck = [];

    if(!$localStorage.eventos[$scope.theEvent].checklists == undefined){
        console.log("segue vazio");
    }else{
        var bjec = {};
        bjec = $localStorage.eventos[$scope.theEvent].checklists;
        angular.forEach(bjec, function(shops, key){
            $scope.listCheck[key] = angular.copy(shops);
        });
        console.log("vai")
        console.log(bjec)        
    }

    document.addEventListener("deviceready", function () {
         $scope.connec = $cordovaNetwork.isOnline();
         console.log($scope.connec);
    });

    $scope.showTaskPrompt = function() {
        var newShop = {
            item: '',
            done: false
        };  

        $scope.newShop = newShop;
        $scope.openModal(1);
    };

     $scope.saveTask = function(indice) {
        if($scope.newShop.item != ''){
            console.log("gravou")
            $scope.listCheck.push($scope.newShop);
            $scope.closeModal(indice);
            $scope.insertIntoObj();
        }else{
            Utils.alertshow("Erro", "Indique o nome do item")
        }
    };

    $scope.cancelTask = function(indice){
        $scope.closeModal(indice);
    };

///////////////////////////////////////////////////////////////////////////////////

    $scope.explainMe = function(state){
        state.done = true;
        $localStorage.eventos[$scope.theEvent].checklists = $scope.listCheck;
        $scope.insertIntoObj();
    };

    $scope.removeMe = function(itim){
        console.log(itim.item);
        var aquele = $scope.listCheck.indexOf(itim);
        console.log(aquele)
        $scope.listCheck.splice(aquele, 1);
        $scope.insertIntoObj();
    };


 $scope.delEvento = function(){
     // A confirm dialog
     var confirmPopup = $ionicPopup.confirm({
       title: 'Deletar',
       template: 'Tem certeza que quer deletar?',
       cssClass: 'my-popup',
       buttons: [
         { text: 'Não',
            type: 'button-assertive myred',
          },
       {
         text: 'Sim',
         type: 'button-positive-900',
         onTap: function() { 
           console.log('You are sure');
         $scope.removefromUpdate(2)        
         }
       }
      ]
     })

     confirmPopup.then(function(res) {
      console.log(res)
       if(res) {

       } else {
         console.log('You are not sure');
       }
     });
  };         
   
    $scope.removefromUpdate = function(indice){
        $scope.listCheck.splice($scope.indiceMod, 1);
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    };

     $scope.doneClicked = function(index, item){
        console.log(index);
        console.log(item);
        if($scope.listCheck[index].done === false){
            $scope.listCheck[index].done = true;
        }else{
            $scope.listCheck[index].done = false
        };
        $scope.insertIntoObj();
    };

     $scope.updateClicked = function(){
        if($scope.modifItem.done === false){
            $scope.modifItem.done = true;
        }else{
            $scope.modifItem.done = false;
        };
    };

    $scope.grave_me = function(objeto){
        if($scope.connec){
            console.log("vou gravar na internet")
            planFactory.insertChecklist($localStorage.usuario.uid, $scope.theEvent, objeto);
        }else{
            console.log("não vou gravar na internet")
            ionicToast.show('Não há conexão com a internet. Os dados serão estocados somente no aparelho até o próximo salvamento', 'middle', false, 3000);
        };

    };

    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        angular.forEach($scope.listCheck, function(shopes, keyo){
            bje[keyo] = $scope.listCheck[keyo];
            bje = angular.copy(bje);
            console.log(bje)
        });
        $localStorage.eventos[$scope.theEvent].checklists = bje;
        //ionicToast.show('Sucesso!', 'middle', false, 3000);
        $scope.grave_me(bje);        
    };     

    $scope.editTask = function(index, obito){
        $scope.indiceMod = index;
        $scope.savedItem = obito.item
        $scope.savedStatus = obito.done;
        $scope.modifItem = {};
        $scope.backupItem = {};
        $scope.modifItem = obito;       
        $scope.backupItem = $scope.modifItem;
        console.log("entrou")
        console.log($scope.modifItem)        
        $scope.openModal(2)
    };

    $scope.notUpdateTask = function(indice, item){
        console.log("saiu sem salvar");
        $scope.modifItem.item = $scope.savedItem;
        $scope.modifItem.done = $scope.savedStatus;
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        console.log($scope.backupItem);
        console.log($scope.modifItem);
        delete $scope.backupItem;
        delete $scope.modifItem;
        $scope.closeModal(indice);
    }

    $scope.updateTask = function(indice) {
        console.log("saiu")
        console.log($scope.modifItem)
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    };


     $ionicModal.fromTemplateUrl('templates/auxiliar/inserechecklist.html', {
      id: '1',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal1 = modal;
   });

    $ionicModal.fromTemplateUrl('templates/auxiliar/modificachecklist.html', {
      id: '2',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal2 = modal;
   });
    
    
   $scope.openModal = function(index) {
      if (index == 1){
       $scope.modal1.show();
      }else{
       $scope.modal2.show();
      }      
   };
    
   $scope.closeModal = function(index) {
      if (index == 1){
       $scope.modal1.hide();
      }else{
       $scope.modal2.hide();
      }
   
   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal1.remove();
      $scope.modal2.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });


}; // fim da função
})(); // fim do documento