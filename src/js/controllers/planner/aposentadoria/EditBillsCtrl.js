(function() {
'use_strict';

angular.module('app').controller('EditBillsCtrl', EditBillsCtrl);

function EditBillsCtrl($scope, $rootScope, ionicToast, $ionicModal, $localStorage, $cordovaNetwork, planFactory, Utils, $ionicPopup){

   $scope.$parent.showHeader();

    console.log($rootScope.eventoDaVez);
    $scope.nome = $rootScope.eventoDaVez.name;
    $scope.theEvent = $rootScope.eventoDaVez.ID;
	
	$scope.valorTotal = 0;
	$scope.valorReceitas = 0;
	$scope.valorDespesas = 0;

    $scope.listCheck = [];	

    if($localStorage.eventos[$scope.theEvent].orcamento){
        var bjec = {};
        bjec = $localStorage.eventos[$scope.theEvent].orcamento;
        angular.forEach(bjec, function(shops, key){

            $scope.listCheck[key] = angular.copy(shops);
            //aqui coloca as somas
        });
        console.log("vai")
        console.log(bjec)

    }else{
        console.log("segue vazio")
    }
        document.addEventListener("deviceready", function () {
         $scope.connec = $cordovaNetwork.isOnline()
         console.log($scope.connec)
        }); 
       

	$scope.casaGrana = function(){
		console.log("clickw")
	        var newBudget = {
	        valor: 0,
	        descrip: '',
	        lancado: 'receita'
	    };
	    $scope.newBudget = newBudget;
	    $scope.openModal(1);
	}	

	$scope.retiraGrana = function(){
		console.log("click")
	        var newBudget = {
	        valor: 0,
	        descrip: '',
	        lancado: 'despesa'
	    };
	    $scope.newBudget = newBudget;
	    $scope.openModal(2);	
	}

	$scope.totalMoney = function(){
		console.log("money!!!!")
		$scope.valorTotal = 0;
		$scope.valorTotal = ($scope.valorReceitas - $scope.valorDespesas)
	};

    $scope.saveTask = function(indice) {
        if($scope.newBudget.descrip == ""){
          Utils.alertshow("Erro", "Faltou a descrição da despesa")        
        }else if($scope.newBudget.valor == 0){
          Utils.alertshow("Erro", "Valor não identificado")   
        }else{
          console.log("gravou")
          $scope.listCheck.push($scope.newBudget);
          $scope.closeModal(indice);
          $scope.insertIntoObj();
        }
    };

    $scope.cancelTask = function(indice) {
        $scope.closeModal(indice);
    };


    $scope.explainMe = function(state){
        state.done = true;
        $localStorage.eventos[$scope.theEvent].orcamento = $scope.listCheck;
        $scope.insertIntoObj();
    }

  $scope.delEvento = function(itex){
     // A confirm dialog
     var confirmPopup = $ionicPopup.confirm({
       title: 'Deletar',
       template: 'Tem certeza que quer deletar?',
       cssClass: 'my-popup',
       buttons: [
         { text: 'Não',
            type: 'button-assertive myred',
          },
       {
         text: 'Sim',
         type: 'button-positive-900',
         onTap: function() { 
           console.log('You are sure');
         $scope.removeMe(itex)        
         }
       }
      ]
     })

     confirmPopup.then(function(res) {
      console.log(res)
       if(res) {

       } else {
         console.log('You are not sure');
       }
     });
  };   


    $scope.removeMe = function(itim){
        //$scope.listCheck.splice(indi, 1)
        var aquele = $scope.listCheck.indexOf(itim)
        console.log(aquele)
        $scope.listCheck.splice(aquele, 1)
        $scope.insertIntoObj();
    }

    $scope.removefromUpdate = function(indice){
        //$scope.listCheck.splice(indi, 1)
        $scope.listCheck.splice($scope.indiceMod, 1)
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    }    

    $scope.doneClicked = function(index, item){
        console.log(index);
        console.log(item);
        if($scope.listCheck[index].done === false){
            $scope.listCheck[index].done = true;
        }else{$scope.listCheck[index].done = false}
        $scope.insertIntoObj();
    };

     $scope.updateClicked = function(){
        if($scope.modifItem.done === false){
            $scope.modifItem.done = true;
        }else{$scope.modifItem.done = false}
        //$scope.insertIntoObj();
    };   

    $scope.grave_me = function(objeto) {
        // body...
        if($scope.connec){
            console.log("vou gravar na internet")
            planFactory.insertBudget($localStorage.usuario.uid, $scope.theEvent, objeto)
        }else{
            console.log("não vou gravar na internet")
            ionicToast.show('Não há conexão com a internet. Os dados serão estocados somente no aparelho até o próximo salvamento', 'middle', false, 3000);
        }

    }   


    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        angular.forEach($scope.listCheck, function(shopes, keyo){
            bje[keyo] = $scope.listCheck[keyo];
            bje = angular.copy(bje);
            console.log(bje)
        });
        $localStorage.eventos[$scope.theEvent].orcamento = bje;
        //ionicToast.show('Sucesso!', 'middle', false, 3000);
        //aqui insere a conta
        $scope.somadasPartes();
        $scope.grave_me(bje);        
    };


    $scope.somadasPartes = function(){
    	console.log("SOMA DAS PARTES")
		$scope.valorReceitas = 0;
		$scope.valorDespesas = 0;    	
        var summ = {};
        summ = $localStorage.eventos[$scope.theEvent].orcamento;
        angular.forEach(summ, function(shops, key){
        	if(shops.lancado === "receita"){
        		console.log("entrou "+shops.valor);
        		$scope.valorReceitas = $scope.valorReceitas + shops.valor;
        	}
        	if(shops.lancado === "despesa"){
        		console.log("saiu "+shops.valor);
        		$scope.valorDespesas = $scope.valorDespesas + shops.valor;
        	}
            //aqui coloca as somas
            $scope.totalMoney(); 
        });  	
    };


    $scope.editMoneyTask = function(obito){
        var index = $scope.listCheck.indexOf(obito)
        $scope.indiceMod = index;
        $scope.modifItem = {};
        $scope.modifItem = obito;
        console.log("entrou")
        console.log($scope.modifItem)
        if($scope.modifItem.lancado == "receita"){
          $scope.openModal(3)         
        }
        if($scope.modifItem.lancado == "despesa"){
          $scope.openModal(4)         
        }
    };  

    $scope.updateTask = function(indice) {
        console.log("saiu")
        console.log($scope.modifItem)
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    };

     $ionicModal.fromTemplateUrl('templates/auxiliar/inserereceita.html', {
      id: '1',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal1 = modal;
   });

    $ionicModal.fromTemplateUrl('templates/auxiliar/inseredespesa.html', {
      id: '2',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal2 = modal;
   });

     $ionicModal.fromTemplateUrl('templates/auxiliar/editareceita.html', {
      id: '3',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal3 = modal;
   });

    $ionicModal.fromTemplateUrl('templates/auxiliar/editadespesa.html', {
      id: '4',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal4 = modal;
   });

    
   $scope.openModal = function(index) {
      if (index == 1){
       $scope.modal1.show();
      }
      if (index == 2){
       $scope.modal2.show();
      }
      if (index == 3){
       $scope.modal3.show();
      }
      if (index == 4){
       $scope.modal4.show();
      }      
   };
    
   $scope.closeModal = function(index) {
      if (index == 1){
       $scope.modal1.hide();
      }
      if (index == 2){
       $scope.modal2.hide();
      }
      if (index == 3){
       $scope.modal3.hide();
      }
      if (index == 4){
       $scope.modal4.hide();
      }
   
   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal1.remove();
      $scope.modal2.remove();
      $scope.modal3.remove();
      $scope.modal4.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });

   $scope.somadasPartes();

}; // fim da função
})(); // fim do documento