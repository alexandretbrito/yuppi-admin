(function() {
'use_strict';

angular.module('app').controller('TheEvtCtrl', TheEvtCtrl);

function TheEvtCtrl($scope, $rootScope, $location, ipCookie, accessFactory){

	console.log($rootScope.chooseEvt);
  $scope.editedEvt = {};
  $scope.editedID = $rootScope.chooseEvt;
  $scope.varUser = ipCookie('sessao');

  $scope.starter = function() {
    $scope.dindin = $scope.varUser.pagante;
    var delEvts = accessFactory.pegaEvento($scope.varUser.uid);
    var delEvts2 = delEvts.child($scope.editedID);
      delEvts2.on("value", function(snapshot){
        $scope.editedEvt = snapshot.val();
        $scope.evtType = $scope.editedEvt.evtType;
         $scope.evtPreCad = true;
         if($scope.editedEvt.evtType == "Casamento"){
          $scope.naocasorio = false;
         }else{
          $scope.naocasorio = true;
         }
          if(!$scope.editedEvt.hasGroup){
            $scope.hasGroup = false;
            console.log("hasGroup = "+$scope.hasGroup)
          }else{
            $scope.hasGroup = $scope.editedEvt.hasGroup;
            console.log("hasGroup = "+$scope.hasGroup)
          }
          console.log("asInvited = "+$rootScope.asInvited)
         $scope.nome = $scope.editedEvt.name;
         $rootScope.globalEvtName = $scope.nome;
         console.log($scope.nome)
      });
  }


	$scope.cronSelector = function(){
		if($scope.editedEvt.evtType == "Casamento"){
			console.log("é um casamento!");
			$location.path("/cronograma/cronograma_casamento");
		}else if($scope.editedEvt.evtType == "Festa Infantil"){
			console.log("é um aniversário de criança!");
			$location.path("/cronograma/cronograma_infantil");
		}else{
			console.log("é um evento normal");
			$location.path("/cronograma/cronograma_evento");
		}
	}

  $scope.clickToView = function(){
  };


/*
     $ionicModal.fromTemplateUrl('templates/auxiliar/view_event.html', {
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal1 = modal;
   });
 */   
    
   $scope.goneFly = function(){
      console.log("Gonna fly now!");
      $scope.closeModal()
      $location.path("app/editaevento")
   }


   $scope.makeGroup = function(){

        $scope.editedEvt.hasGroup = true;
        var recoded = angular.copy($scope.editedEvt)
        planFactory.insertEvt($scope.varUser.uid, $scope.editedID, recoded)
        var meuGrupo = {}
        meuGrupo.owner = $scope.varUser.uid;
        meuGrupo.ID = $scope.editedID;
        console.log(meuGrupo)
        planFactory.makeGroup($scope.editedID, meuGrupo)
        Utils.alertshow("Sucesso", "Um grupo foi criado para este evento")
        $scope.hasGroup = true;
   }

    //ionicMaterialInk.displayEffect();	
}; // fim da função
})(); // fim do documento