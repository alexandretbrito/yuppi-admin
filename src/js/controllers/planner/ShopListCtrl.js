(function() {
'use_strict';

angular.module('app').controller('ShopListCtrl', ShopListCtrl);
//
function ShopListCtrl($scope, $rootScope, $localStorage, planFactory, accessFactory, Utils, ipCookie){

    console.log($rootScope.chooseEvt);
    $scope.editedID = $rootScope.chooseEvt;
    $scope.varUser = ipCookie('sessao');   
    var objTodo = {}
    $scope.listCheck = [];


    $scope.starter = function(){
      console.log("starter")
      objTodo = {}
      var trans = accessFactory.pegaShopList($scope.varUser.uid);
      var traste = trans.child($scope.editedID);
      traste.on("value", function(snapshot){
        objTodo = snapshot.val();
        getTasks();
      });
    }

    function getTasks() {
      // body...
      if(objTodo != {}){
          var bjec = {};
          bjec = objTodo;
          angular.forEach(bjec, function(tods, key){

              $scope.listCheck[key] = angular.copy(tods);
              //console.log(bjec)
          });
      }else{
          console.log("segue vazio")
      }
      $scope.$apply();
    }
        $scope.showTaskPrompt = function() {
            var newShop = {
                item: '',
                done: false
            };  

            $scope.newShop = newShop;
        };

        $scope.saveTask = function() {
          if($scope.newShop.item != ''){
              console.log("gravou")
              $scope.listCheck.push($scope.newShop);
              $scope.insertIntoObj();
          }else{
              Utils.alertshow("Erro", "Indique o nome do item")
          }
          $('#newCompra').modal('hide')
        };          

///////////////////////////////////////////////////////////////////////////////////

    $scope.removeMe = function(itim){
      console.log(itim.item)
      var aquele = $scope.listCheck.indexOf(itim)
      console.log(aquele);
      $scope.listCheck.splice(aquele, 1)
      console.log($scope.listCheck);
      $scope.$apply();
        $scope.insertIntoObj();
    }

    $scope.removefromUpdate = function(indice){
        console.log($scope.listCheck[indice])
        $scope.listCheck.splice(indice, 1)
        $scope.insertIntoObj();
    }    

    $scope.doneClicked = function(index, item){
        console.log(index);
        console.log(item);
        if($scope.listCheck[index].done === false){
            $scope.listCheck[index].done = true;
        }else{$scope.listCheck[index].done = false}
        $scope.insertIntoObj();
    };

     $scope.updateClicked = function(){
        if($scope.modifItem.done == false){
            $scope.modifItem.done = true;
        }else{$scope.modifItem.done = false}
    };  

    $scope.grave_me = function(objeto) {
        // body...
            planFactory.insertShop($scope.varUser.uid, $scope.editedID, objeto)
    }   

    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        angular.forEach($scope.listCheck, function(todes, keyo){
            bje[keyo] = $scope.listCheck[keyo];
            bje = angular.copy(bje);
            console.log(bje)
        });
        $scope.grave_me(bje);        
    };

    $scope.editTask = function(index, obito){
        $scope.indiceMod = index;
        $scope.savedItem = obito.item
        $scope.savedStatus = obito.done;
        $scope.modifItem = {};
        $scope.backupItem = {};
        $scope.modifItem = obito;       
        $scope.backupItem = $scope.modifItem;
        console.log("entrou")
        console.log($scope.modifItem)        
    };

    $scope.notUpdateTask = function(item){
        console.log("saiu sem salvar");
        $scope.modifItem.item = $scope.savedItem;
        $scope.modifItem.done = $scope.savedStatus;
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        console.log($scope.backupItem);
        console.log($scope.modifItem);
        delete $scope.backupItem;
        delete $scope.modifItem;
    }

    $scope.updateTask = function() {
        console.log("saiu")
        console.log($scope.modifItem)
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        $scope.insertIntoObj();
        $('#renewCompra').modal('hide')
    };

}; // fim da função
})(); // fim do documento