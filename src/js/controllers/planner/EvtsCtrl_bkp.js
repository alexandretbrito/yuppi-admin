(function() {
'use_strict';

angular.module('app').controller('EvtsCtrl', EvtsCtrl);

function EvtsCtrl($scope, $rootScope, $location, $sessionStorage, Utils, $timeout, planFactory, accessFactory){

    $scope.tuttiEventi = [];
    $scope.timePaster = false;
	$scope.temEventos = false;
	var eventolandia = {};
	$scope.calendar = {}

	$scope.timePaster = false;

	$scope.usuarioAtivo = $sessionStorage.usuario;	
	console.log($scope.usuarioAtivo)
	console.log("estou em eventos")

	showContent()

	function starter() {
		// body...
		$rootScope.onEvent = false;
		var evPath = accessFactory.pegaEventList($scope.usuarioAtivo.uid);
			eventolandia = {};
			evPath.once('value').then(function(snapshot){
			eventolandia = snapshot.val();
			evalEvents();
		});
	}


	$scope.$on("$destroy", function(event, data){
			$scope.timePaster = false;
			console.log("the events are leaving the building...")		
	})	

		$scope.newEvt = {};
		$rootScope.allEvents = {};	


function evalEvents() {
	console.log("evalEvents")
		var i=0
		var bjec = {};
        var agora = new Date().getTime();
        $scope.tuttiEventi = [];
        bjec = eventolandia;
        angular.forEach(bjec, function(shops, key){
			$scope.tuttiEventi[i] = angular.copy(shops);
			$scope.tuttiEventi[i].compareDate = datetoalign($scope.tuttiEventi[i].evtDay, $scope.tuttiEventi[i].evtHour);
			$scope.tuttiEventi[i].past = datetofade($scope.tuttiEventi[i].compareDate, agora);
            i++;
        });
        console.log("vai")
        console.log($scope.tuttiEventi);
        $scope.$apply();
        $scope.calendar.eventSource = bringEvents();
        console.log($scope.calendar.eventSource);
}


	$scope.dia = function(evento){
		console.log("chegou o dia")
		var dengo = new Date(evento.evtDay);
		return dengo;
	}

	function datetoalign(evtdate, hora){
		var dateArr = [];
		dateArr = evtdate.split("/");
		var supertime = dateArr[2]+"-"+dateArr[1]+"-"+dateArr[0]+"T"+hora+":00";
		var numTime = new Date(supertime).getTime();
		return numTime;
	}

	function datetofade(datecomp, datenow){
		var bool_state;
		if(datecomp > datenow){
			bool_state = true;
		}else{
			bool_state = false;
		}
		return bool_state;
	}

	$scope.clicked = function(eventissimo){
		var daVez = {};
		daVez = eventissimo;
		console.log(daVez);
		$rootScope.globalEvtName = eventissimo.name;
		$rootScope.chooseEvt = eventissimo.ID;
		$rootScope.asInvited = false;
		$rootScope.onEvent = true;
		$location.path('eventos/eventounico');
	};

	$scope.delEvento = function(evento){
		var daVez = {};
		daVez = evento;

	};

	$scope.mandapraNovo = function(evento){
		$location.path('/eventos/novoevento');
	};

	$scope.deletaEvento = function(eventer){
		console.log(eventer)
		//delete $localStorage.eventos[eventer.uid];
		planFactory.removeEvt($scope.usuarioAtivo.uid , eventer.ID);
		starter();
	}

	$scope.clickToView = function(eventissimo){
		var daVez = {};
		daVez = eventissimo;
		console.log(daVez);
		$rootScope.eventoDaVez = daVez;
 		$scope.evtInfo = $rootScope.eventoDaVez;
		//$scope.openModal();
	};

   $scope.goneFly = function(evita){
      console.log("Gonna fly now!");
      $rootScope.chooseEvt = evita.ID
      console.log($rootScope.chooseEvt);
      $location.path("/eventos/editar_evento")
   }


   function showContent() {
   	// body...
	   $timeout(function(){
			$scope.timePaster = true;
			console.log("demorou...")
			starter();
	   }, 1200);
   }

	$scope.testClick= function(){
		alert("Acertou, miserávi!!!")
	}

	function bringEvents(){
			console.log("bring events")
			console.log(eventolandia)
			var i=0
	        var bjec = {};
			var events = [];
	        bjec = eventolandia;
	        angular.forEach(bjec, function(shops, key){
	        	var objent = {}
	        	objent.title = shops.name;
	        	objent.allDay = true;
	        	//
	        	var evtdate = shops.evtDay
	        	var brokeDate = [];
	        	brokeDate = evtdate.split("/")
	        	//
	        	objent.startTime = new Date(Date.UTC(brokeDate[2], brokeDate[1]-1, brokeDate[0], 3, 0, 0));
	        	objent.endTime = new Date(Date.UTC(brokeDate[2], brokeDate[1]-1, brokeDate[0], 3, 0, 0));
	        	objent.ID = shops.ID;
	        	objent.evtHour = shops.evtHour;
	        	//console.log(objent.startTime)
	        	events[i] = objent;
            i++;
	        });
	        return events;
	}


}; // fim da função
})(); // fim do documento