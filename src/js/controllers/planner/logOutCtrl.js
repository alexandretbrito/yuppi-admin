(function() {
'use strict';

angular.module('app').controller('logOutCtrl', logOutCtrl)

function logOutCtrl($scope, $rootScope, $timeout, $location, FireAuth, $localStorage, ipCookie) {
      // body...
      $rootScope.$isSigned = false;
      console.log("passando pelo logout")
      // desligar session
      $localStorage.$reset();
      ipCookie.remove('sessao')
      //desligar autenticação
      FireAuth.logout();

      $location.path("/login")
};// fim da função
})();// fim do arquivo