(function() {
'use_strict';

angular.module('app').controller('BillsCtrl', BillsCtrl);

function BillsCtrl($scope, $rootScope, $localStorage,planFactory, accessFactory, Utils, ipCookie){

  $scope.editedID = $rootScope.chooseEvt;
  $scope.varUser = ipCookie('sessao');   

	$scope.valorTotal = 0;
	$scope.valorReceitas = 0;
	$scope.valorDespesas = 0;
  var balanco = {};

    $scope.listCheck = [];	

  $scope.starter = function(){
    console.log("starter");
      var trans = accessFactory.pegaBalanco($scope.varUser.uid);
      var traste = trans.child($scope.editedID);
      traste.on("value", function(snapshot){
        balanco = snapshot.val();
        if(balanco){
        var bjec = {};
        bjec = balanco;
        angular.forEach(bjec, function(shops, key){
            $scope.listCheck[key] = angular.copy(shops);
            $scope.somadasPartes();
        });
        }else{
          console.log("segue vazio")
          $scope.somadasPartes();
        }
              $scope.$apply()
      });
  };   


	$scope.casaGrana = function(){
		console.log("clickw")
	        var newBudget = {
	        valor: 0,
	        descrip: '',
	        lancado: 'receita'
	    };
	    $scope.newBudget = newBudget;
	    $('#novolancamento').modal('show')
	}	

	$scope.retiraGrana = function(){
		console.log("click")
	        var newBudget = {
	        valor: 0,
	        descrip: '',
	        lancado: 'despesa'
	    };
	    $scope.newBudget = newBudget;
	    $('#novolancamento').modal('show')	
	}

	$scope.totalMoney = function(){
		console.log("money!!!!")
		$scope.valorTotal = 0;
		$scope.valorTotal = ($scope.valorReceitas - $scope.valorDespesas)
	};

    $scope.saveTask = function() {
        if($scope.newBudget.descrip == ""){
          Utils.alertshow("Erro", "Faltou a descrição")        
        }else if($scope.newBudget.valor == 0){
          Utils.alertshow("Erro", "Valor não identificado")   
        }else{
          console.log("gravou")
          $scope.listCheck.push($scope.newBudget);
          $('#novolancamento').modal('hide');
          $scope.somadasPartes();
          $scope.insertIntoObj();
        }
    };

    $scope.cancelTask = function(indice) {
        $scope.closeModal(indice);
    };

    $scope.removeMe = function(itim){
        //$scope.listCheck.splice(indi, 1)
        var aquele = $scope.listCheck.indexOf(itim)
        console.log(aquele)
        $scope.listCheck.splice(aquele, 1)
        $scope.insertIntoObj();
    }

    $scope.removefromUpdate = function(indice){
        //$scope.listCheck.splice(indi, 1)
        $scope.listCheck.splice($scope.indiceMod, 1)
        $scope.closeModal(indice);
        $scope.insertIntoObj();
    }    

    $scope.doneClicked = function(index, item){
        console.log(index);
        console.log(item);
        if($scope.listCheck[index].done === false){
            $scope.listCheck[index].done = true;
        }else{$scope.listCheck[index].done = false}
        $scope.insertIntoObj();
    };

     $scope.updateClicked = function(){
        if($scope.modifItem.done === false){
            $scope.modifItem.done = true;
        }else{$scope.modifItem.done = false}
        //$scope.insertIntoObj();
    };   

    $scope.grave_me = function(objeto) {
        // body...
            planFactory.insertBudget($scope.varUser.uid, $scope.editedID, objeto)
    }   


    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        angular.forEach($scope.listCheck, function(shopes, keyo){
            bje[keyo] = $scope.listCheck[keyo];
            bje = angular.copy(bje);
            console.log(bje)
        });
        $scope.somadasPartes();
        $scope.grave_me(bje);        
    };


    $scope.somadasPartes = function(){
    	console.log("SOMA DAS PARTES")
		$scope.valorReceitas = 0;
		$scope.valorDespesas = 0;    	
        var summ = {};
        summ = balanco;
        angular.forEach(summ, function(shops, key){
        	if(shops.lancado === "receita"){
        		console.log("entrou "+shops.valor);
        		$scope.valorReceitas = $scope.valorReceitas + shops.valor;
        	}
        	if(shops.lancado === "despesa"){
        		console.log("saiu "+shops.valor);
        		$scope.valorDespesas = $scope.valorDespesas + shops.valor;
        	}
            //aqui coloca as somas
            $scope.totalMoney(); 
        });  	
    };


    $scope.editMoneyTask = function(obito){
        console.log(obito);
        var index = $scope.listCheck.indexOf(obito)
        $scope.indiceMod = index;
        console.log($scope.indiceMod)
        $scope.modifItem = {};
        $scope.modifItem = obito;
        console.log("entrou")
        console.log($scope.modifItem)
        $('#editarlancamento').modal('show');
    };  

    $scope.updateTask = function(indice) {
        console.log("saiu")
        console.log($scope.modifItem)
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
          $('#editarlancamento').modal('hide');
          $scope.somadasPartes();
        $scope.insertIntoObj();
    };

    /*

     $ionicModal.fromTemplateUrl('templates/auxiliar/inserereceita.html', {
      id: '1',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal1 = modal;
   });

    $ionicModal.fromTemplateUrl('templates/auxiliar/inseredespesa.html', {
      id: '2',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal2 = modal;
   });

     $ionicModal.fromTemplateUrl('templates/auxiliar/editareceita.html', {
      id: '3',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal3 = modal;
   });

    $ionicModal.fromTemplateUrl('templates/auxiliar/editadespesa.html', {
      id: '4',
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal4 = modal;
   });

    
   $scope.openModal = function(index) {
      if (index == 1){
       $scope.modal1.show();
      }
      if (index == 2){
       $scope.modal2.show();
      }
      if (index == 3){
       $scope.modal3.show();
      }
      if (index == 4){
       $scope.modal4.show();
      }      
   };
    
   $scope.closeModal = function(index) {
      if (index == 1){
       $scope.modal1.hide();
      }
      if (index == 2){
       $scope.modal2.hide();
      }
      if (index == 3){
       $scope.modal3.hide();
      }
      if (index == 4){
       $scope.modal4.hide();
      }
   
   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal1.remove();
      $scope.modal2.remove();
      $scope.modal3.remove();
      $scope.modal4.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });

   */

}; // fim da função
})(); // fim do documento