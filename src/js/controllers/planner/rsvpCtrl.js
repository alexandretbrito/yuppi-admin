(function() {
'use_strict';

angular.module('app').controller('rsvpCtrl', rsvpCtrl);

function rsvpCtrl($scope, $rootScope, $localStorage, planFactory, accessFactory, Utils, ipCookie, $window, $timeout){

    console.log($rootScope.chooseEvt);
    $scope.editedID = $rootScope.chooseEvt;
    $scope.varUser = ipCookie('sessao');   
    var objTodo = {}
    $scope.listCheck = [];
    $scope.readMethod = "readAsBinaryString";
    $scope.loaded = false;

	$scope.onReaded = function( e, file ){

	  $scope.result = e.target.result;
	  $scope.file = file;
	  $scope.loaded = true;
	};

    $scope.prepareImport = function(){
    	$scope.loaded = false;
    }	

	$scope.importGuests = function(){

	  var lines = [];

	  var pits = $scope.result.toString();
	
	  lines = pits.split("\n");

    // somente pegar a quantidade de áreas
    var headers = lines[0].split(";");
    //retirar o cabeçalho
    lines.splice(0,1);

	  var finalCount = lines.length

	  var result = [];

	  //var headers=lines[0].split(",");

	  for(var i=0;i<lines.length;i++){
	  	if(lines[i] != ""){

    		  var obj = {};
          var currentline = lines[i].split(";")

    		    obj.nome = currentline[0];
            obj.convidados = Number(currentline[1]);
    		    obj.done = false;


		 console.log(obj)
		 $scope.listCheck.push(obj)
		  //result.push(obj)
		  //console.log(result)
		  }
		  if(i == finalCount-1){
		  	console.log("processo completo")
		  	$scope.insertIntoObj();
		  }

	  }

		$('#importGuest').modal('hide')
	}

    $scope.starter = function(){
      console.log("starter")
      objTodo = {}
      var trans = accessFactory.pegaGuestList($scope.varUser.uid);
      var traste = trans.child($scope.editedID);
      traste.on("value", function(snapshot){
        objTodo = snapshot.val();
        if(objTodo !== null){
        getGuests();
    	}else{
    	console.log("vazio");
			$window.alert("Não há convidados registrados. Importe um arquivo CSV somente com nome do convidado na primeira coluna e a sua quantidade de convites em outra coluna para registrar.")	
    	}
      });
    }

    function getGuests() {
      // body...
      console.log("//////////////////")
      console.log(objTodo)
      console.log("//////////////////")
      var numtemNada = []
      if(objTodo != {}){
          var bjec = {};
          bjec = objTodo;
          angular.forEach(bjec, function(tods, key){
              numtemNada[key] = angular.copy(tods);
              console.log(numtemNada[key])
          });
      }else{
          console.log("segue vazio")
      }

      $timeout(function(){
        $scope.listCheck = numtemNada;
      }, 1000);
      //$scope.$apply();
    }

    $scope.showTaskPrompt = function() {
            var newGuest = {
                nome: '',
                convidados: 1,
                done: false
            };  

            $scope.newGuest = newGuest;
        };

        $scope.saveTask = function() {
          $('#new_guest').modal('hide')
          if($scope.newGuest.nome != ''){
              console.log("gravou")
              $scope.listCheck.push($scope.newGuest);
              $scope.insertIntoObj();
          }else{
              Utils.alertshow("Erro", "Indique o nome do convidado")
          }

        };

///////////////////////////////////////////////////////////////////////////////////

    $scope.editTask = function(obito){
        $scope.indiceMod = $scope.listCheck.indexOf(obito);
        $scope.modifItem = {};
        backupItem = {};
        $scope.modifItem = obito;
        $localStorage.backup = {}
        $localStorage.backup.noms = obito.nome;
        $localStorage.backup.convs = obito.convidados;
        $localStorage.backup.dons = obito.done;
        console.log("entrou")
        console.log($scope.indiceMod)
        console.log($localStorage.backup)
    };

    $scope.notUpdateTask = function(){
        delete $scope.modifItem;
        console.log("saiu sem salvar");
        console.log($localStorage.backup);
        $scope.listCheck[$scope.indiceMod].nome = $localStorage.backup.noms;
        $scope.listCheck[$scope.indiceMod].convidados = $localStorage.backup.convs;
        $scope.listCheck[$scope.indiceMod].done = $localStorage.backup.dons;
        $localStorage.$reset();
    }

    $scope.updateTask = function() {
        console.log("saiu")
        console.log($scope.modifItem)
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        $scope.insertIntoObj();
        $localStorage.$reset();
        $('#editGuest').modal('hide')
    };                  

///////////////////////////////////////////////////////////////////////////////////

    $scope.removeMe = function(itim){
    	console.log(itim.item)
    	var aquele = $scope.listCheck.indexOf(itim)
    	console.log(aquele);
    	$scope.listCheck.splice(aquele, 1)
      console.log($scope.listCheck);
      $scope.$apply();
        $scope.insertIntoObj();
    }

      $scope.doneClicked = function(item){
      	var index = $scope.listCheck.indexOf(item)
        console.log(item);
        if($scope.listCheck[index].done === false){
            $scope.listCheck[index].done = true;
        }else{$scope.listCheck[index].done = false}
        $scope.insertIntoObj();
    };  

    $scope.grave_me = function(objeto) {
            planFactory.insertGuest($scope.varUser.uid, $scope.editedID, objeto)
    }   

    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        angular.forEach($scope.listCheck, function(todes, keyo){
            bje[keyo] = $scope.listCheck[keyo];
            bje = angular.copy(bje);
            console.log(bje)
        });
        $scope.grave_me(bje);        
    };

///////////////////////////////////////////////////////////////////////////////////

    $scope.totalize_me = function(){
      var numino = 0;
        angular.forEach($scope.listCheck, function(todes, keyo){
            numino = numino + todes.convidados
        });
        return numino;
    }

    $scope.confirme_me = function(){
      var numino2 = 0;
        angular.forEach($scope.listCheck, function(todes, keyo){
            if(todes.done == true){
              numino2 = numino2 + todes.convidados
            }
        });
        return numino2;
    }

    $scope.differ_me = function(){
      var parte_a = $scope.totalize_me();
      var parte_b = $scope.confirme_me();
        return parte_a - parte_b;
    }


}; // fim da função
})(); // fim do documento