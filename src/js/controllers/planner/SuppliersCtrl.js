(function() {
'use_strict';

angular.module('app').controller('SuppliersCtrl', SuppliersCtrl);

function SuppliersCtrl($scope, $rootScope, $localStorage, planFactory, accessFactory, Utils, ipCookie){

    console.log($rootScope.chooseEvt);
    $scope.editedID = $rootScope.chooseEvt;
    $scope.varUser = ipCookie('sessao');   
    var objTodo = {}
    $scope.listCheck = [];

    $scope.tipolog = [
      {tips: "Tipo de Fornecedor"},
      {tips: "Bolo"},
      {tips: "Doce"},
      {tips: "Bebidas"},
      {tips: "Convite"},
      {tips: "Móveis"},
      {tips: "Itens de Decoração"},
      {tips: "Iluminação"},
      {tips: "DJ"},
      {tips: "Foto"},
      {tips: "Filme"},
      {tips: "Lembranças"},
      {tips: "Buffet"},
      {tips: "Animação"},
      {tips: "Atração Musical"},
      {tips: "Vestido"},
      {tips: "Flores"},
      {tips: "Jóias"},
      {tips: "Maquiagem"},
      {tips: "Celebrante"},
      {tips: "Outro"}
    ];

    $scope.evtPreCad = true;
    $scope.otherEvtType = "";
    $scope.evtType = {};
    $scope.mySelect = $scope.tipolog[0];

    $scope.starter = function(){
      console.log("starter")
      objTodo = {}
      var trans = accessFactory.pegaFornecedores($scope.varUser.uid);
      var traste = trans.child($scope.editedID);
      traste.on("value", function(snapshot){
        objTodo = snapshot.val();
        getTasks();
      });
    }

    function getTasks() {
      // body...
      if(objTodo != {}){
          var bjec = {};
          bjec = objTodo;
          angular.forEach(bjec, function(tods, key){

              $scope.listCheck[key] = angular.copy(tods);
              //console.log(bjec)
          });
      }else{
          console.log("segue vazio")
      }
      $scope.$apply();
    }
        $scope.showTaskPrompt = function() {
            var newShop = {
                nome: '',
                tel: '',
                email: '', 
                tipo: ''
            };  

            $scope.newShop = newShop;
        };

    $scope.showTask = function(index, obito){
        //ionicToast.show('Este botão edita o item de compra', 'middle', false, 3000);
        $scope.indiceMod = index;
        $scope.modifItem = {};
        $scope.modifItem = obito;
        console.log("entrou")
        console.log($scope.modifItem);
    };

        $scope.saveTask = function() {
         if($scope.newShop.nome == "" || $scope.evtType == "Tipo de Fornecedor" || $scope.evtType == ""  || $scope.evtType == ""){
            Utils.alertshow("Erro", "Algum item não foi preenchido. Verifique e tente novamente.")
         }else{
            if($scope.evtType !== "Outro"){
              $scope.newShop.tipo = $scope.evtType;
            }
            console.log("gravou")
            $scope.listCheck.push($scope.newShop);
            $scope.insertIntoObj();
            $('#newSupplier').modal('hide')
         };
        };          

///////////////////////////////////////////////////////////////////////////////////

    $scope.removeMe = function(itim){
      console.log(itim.item)
      var aquele = $scope.listCheck.indexOf(itim)
      console.log(aquele);
      $scope.listCheck.splice(aquele, 1)
      console.log($scope.listCheck);
      $scope.$apply();
        $scope.insertIntoObj();
    }

    $scope.removefromUpdate = function(indice){
        console.log($scope.listCheck[indice])
        $scope.listCheck.splice(indice, 1)
        $scope.insertIntoObj();
    }    

    $scope.doneClicked = function(index, item){
        console.log(index);
        console.log(item);
        if($scope.listCheck[index].done === false){
            $scope.listCheck[index].done = true;
        }else{$scope.listCheck[index].done = false}
        $scope.insertIntoObj();
    };

     $scope.updateClicked = function(){
        if($scope.modifItem.done == false){
            $scope.modifItem.done = true;
        }else{$scope.modifItem.done = false}
    };  

    $scope.grave_me = function(objeto) {
        // body...
        planFactory.insertSup($scope.varUser.uid, $scope.editedID, objeto);
        $scope.evtPreCad = true;
        $scope.mySelect = $scope.tipolog[0].tips;
        $scope.otherEvtType = "";
        $scope.evtType = "";
    }   

    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        angular.forEach($scope.listCheck, function(todes, keyo){
            bje[keyo] = $scope.listCheck[keyo];
            bje = angular.copy(bje);
            console.log(bje)
        });
        $scope.grave_me(bje);        
    };

    $scope.editTask = function(index, obito){
        $scope.indiceMod = index;
        $scope.modifItem = {};
        $scope.modifItem = obito;
        $scope.evtType = $scope.modifItem.tipo;
        console.log("entrou")
        console.log($scope.modifItem);
        $scope.preterChoose();       
    };

    $scope.notUpdateTask = function(item){
        console.log("saiu sem salvar");
        $scope.modifItem.item = $scope.savedItem;
        $scope.modifItem.done = $scope.savedStatus;
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        $scope.mySelect = $scope.tipolog[0];
        console.log($scope.modifItem);
        delete $scope.backupItem;
        delete $scope.modifItem;
    }

    $scope.updateTask = function() {
        console.log("saiu")
        if($scope.evtType != $scope.mySelect){
          console.log("vou trocar")
          $scope.modifItem.tipo = $scope.evtType; 
        }
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        $scope.insertIntoObj();
        $('#renewSupplier').modal('hide')
    };


 $scope.changeEventType = function(sel){
      console.log(sel);
      $scope.evtType = sel.tips;
      console.log($scope.evtType); 
    if($scope.evtType === "Outro"){
      $scope.evtPreCad = false;
      console.log("não está na lista");   
    }
    if($scope.evtType !== "Outro"){
        console.log("está na lista");
       $scope.evtPreCad = true;     
    }else{
      $scope.evtPreCad = false;
      console.log("não está na lista");   
    }
  };

  $scope.preterChoose = function(){
      console.log("preterChoose")
      var dito = $scope.modifItem.tipo;
      var zz = 0;
      angular.forEach($scope.tipolog, function(shops, key){
          if(shops.tips == dito){
             zz++;
             console.log("Yeeeei")
             console.log(key)
             $scope.mySelect = $scope.tipolog[key]; 
             $scope.evtPreCad = true;
          }else{
            $scope.mySelect = $scope.tipolog[0];
            $scope.evtPreCad = false;
          }  
      });
      if(zz != 0){
        $scope.evtPreCad = true;
      }else{
        $scope.evtPreCad = false;
      }
  };



}; // fim da função
})(); // fim do documento