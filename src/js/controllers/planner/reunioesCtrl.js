(function() {
    'use_strict';
    
    angular.module('app').controller('reunioesCtrl', reunioesCtrl);
    
    function reunioesCtrl($scope, $rootScope, $localStorage, accessFactory, planFactory, Utils, $filter, ipCookie){
    
        console.log($rootScope.chooseEvt);
        $scope.editedID = $rootScope.chooseEvt;
        $scope.varUser = ipCookie('sessao');
        var objTodo = {}
        $scope.listCheck = [];
    
    
        $scope.starter = function(){
          console.log("starter")
          objTodo = {}
          var trans = accessFactory.getMeetings($scope.varUser.uid);
          var traste = trans.child($scope.editedID);
          traste.on("value", function(snapshot){
            objTodo = snapshot.val();
            console.log("----------------------") 
            console.log (objTodo)
            getTasks();
          });
        }
    
        function getTasks() {
          // body...
          if(objTodo != {}){
              var bjec = {};
              bjec = objTodo;
              angular.forEach(bjec, function(tods, key){
    
                  $scope.listCheck[key] = angular.copy(tods);
                  //console.log(bjec)
              });
          }else{
              console.log("segue vazio")
          }
          $scope.$apply();
        }
        $scope.showTaskPrompt = function() {
            var newTask = {
                local: '',
                done: false
            };  

			var saveDay = new Date();
		    $scope.itemDate = {
		     	value: saveDay
		   	};

		    $scope.itemTime = {
		     	value: saveDay,
		   	};			   	

		   	console.log($scope.itemDate)
		   	console.log($scope.itemTime)

            $scope.newTask = newTask;
        };          
    
        $scope.primeHour = function(){
			var saveDay = new Date();
			$scope.itemTime = {
	    		value: saveDay
	    	}; 
	
 		}

 		$scope.seeChange01 = function(){
 			$scope.changedDate = true;
 		}

//////////////////////////////////////////////////////////

function engMonth(mes){
var abrev = "";
if(mes === "01"){
	abrev = "Jan"; 
}else if(mes === "02"){
	abrev = "Feb";	
}else if(mes === "03"){
	abrev = "Mar";	
}else if(mes === "04"){
	abrev = "Apr";	
}else if(mes === "05"){
	abrev = "May";	
}else if(mes === "06"){
	abrev = "Jun";	
}else if(mes === "07"){
	abrev = "Jul";	
}else if(mes === "08"){
	abrev = "Aug";	
}else if(mes === "09"){
	abrev = "Sep";	
}else if(mes === "10"){
	abrev = "Oct";	
}else if(mes === "11"){
	abrev = "Nov";	
}else if(mes === "12"){
	abrev = "Dec";	
}

return abrev;

}
    ///////////////////////////////////////////////////////////////////////////////////

    $scope.saveTask = function(indice) {
        if($scope.newTask.local != ''){
            console.log("gravou");
            $scope.newTask.hora = $filter('date')($scope.itemTime.value, 'HH:mm');
            $scope.newTask.dia = $filter('date')($scope.itemDate.value, 'dd/MM/yyyy');

              var shaca = $scope.newTask.dia;
              var utcMes = shaca.split("/");
              var diaCal = utcMes[0];
              var anoCal = utcMes[2];
              var mesCal =  utcMes[1];
              var calId = diaCal+"-"+engMonth(mesCal)+"-"+anoCal;

              $scope.newTask.calId = calId;

            $scope.listCheck.push($scope.newTask);
            $('#newMeeting').modal('hide')
            $scope.insertIntoObj();
        }else{
            Utils.alertshow("Erro", "Indique o local da reunião")
        }
      };          

      $scope.cancelTask = function(indice) {
          $scope.closeModal(indice);
           $scope.itemTime = null;
          $scope.itemDate = null;           
      };    
    
///////////////////////////////////////////////////////////////////////////////////

$scope.removeMe = function(itim){
    console.log(itim.item)
    var aquele = $scope.listCheck.indexOf(itim)
    console.log(aquele);
    $scope.listCheck.splice(aquele, 1)
    console.log($scope.listCheck);
    $scope.$apply();
      $scope.insertIntoObj();
  }

  $scope.removefromUpdate = function(indice){
      console.log($scope.listCheck[indice])
      $scope.listCheck.splice(indice, 1)
      $scope.insertIntoObj();
  }    

  $scope.doneClicked = function(index, item){
      console.log(index);
      console.log(item);
      if($scope.listCheck[index].done === false){
          $scope.listCheck[index].done = true;
      }else{$scope.listCheck[index].done = false}
      $scope.insertIntoObj();
  };

   $scope.updateClicked = function(){
      if($scope.modifItem.done == false){
          $scope.modifItem.done = true;
      }else{$scope.modifItem.done = false}
  };  


      $scope.grave_me = function(objeto) {
        // body...
            planFactory.insertMeeting($scope.varUser.uid, $scope.editedID, objeto)
    }   

    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        angular.forEach($scope.listCheck, function(todes, keyo){
            bje[keyo] = $scope.listCheck[keyo];
            bje = angular.copy(bje);
            console.log(bje)
        });
        $scope.grave_me(bje);        
    };

    $scope.editTask = function(index, obito){
        $scope.indiceMod = index;
        $scope.modifItem = {};
        $scope.backupItem = {};
        $scope.modifItem = obito;       
        $scope.backupItem = $scope.modifItem;
        console.log("entrou")
        console.log($scope.modifItem)        

		var timeSeed = $scope.modifItem.hora;
		var arr = timeSeed.split(':');
		var hr = arr[0];
		var mn = arr[1];
		var saveData = new Date(0);
		saveData.setHours(hr);
		saveData.setMinutes(mn);
		$scope.itemTime = {
	    	value: saveData
		};
		console.log($scope.itemTime.value +" de horas") 

		var diaD = $scope.modifItem.dia;
		var diaArr = diaD.toString().split('/');
		var finalCountDown = new Date(diaArr[2], diaArr[1], diaArr[0]);
	    $scope.itemDate = {
	     	value: finalCountDown
	   	};       
    };

    $scope.notUpdateTask = function(item){
        console.log("saiu sem salvar");
        $scope.modifItem.item = $scope.savedItem;
        $scope.modifItem.done = $scope.savedStatus;
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        console.log($scope.backupItem);
        console.log($scope.modifItem);
        delete $scope.backupItem;
        delete $scope.modifItem;
    }

    $scope.updateTask = function() {
        console.log("saiu")
        console.log($scope.modifItem)
		$scope.modifItem.hora = $filter('date')($scope.itemTime.value, 'HH:mm');
		$scope.modifItem.dia = $filter('date')($scope.itemDate.value, 'dd/MM/yyyy');

				var shaca = $scope.modifItem.dia;
				var utcMes = shaca.split("/");
				var diaCal = utcMes[0];
				var anoCal = utcMes[2];
				var mesCal =  utcMes[1];
				var calId = diaCal+"-"+engMonth(mesCal)+"-"+anoCal;

				$scope.modifItem.calId = calId;

        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        $scope.insertIntoObj();
        $('#renewMeeting').modal('hide')
    };    
    }; // fim da função
    })(); // fim do documento