(function(){
	'use strict';
	/**
	* accessFactory
	* Facilita o caminho para os conteúdo na base de dados
	* Alexandre Brito 2016
	*/
	angular.module('app')
	.factory('accessFactory', accessFactory);

	console.log("acesso!")
	function accessFactory(){
		var userPath = firebase.database().ref("usuarios");
		var eventPath = firebase.database().ref("eventos");
		var todoPath = firebase.database().ref("to_dos");
		var techPath = firebase.database().ref("visita_tecnica");
		var checkPath = firebase.database().ref("checklists");				
		var shopPath = firebase.database().ref("compras");
		var cronoLists = firebase.database().ref("cronogramas");
		var workLists = firebase.database().ref("fornecedores");
		var moneyPath = firebase.database().ref("orcamento");
		var groupPath = firebase.database().ref("grupos");
		var galleryPath = firebase.database().ref("galerias");
		var guestPath = firebase.database().ref("convidados");
		var meetPath = firebase.database().ref("reunioes");
		var nakedPath = firebase.database();		
		
		var accessBack = {
			pegaUsuario: pegaUsuario,
			pegaEvento: pegaEvento,
			pegaEventList: pegaEventList,			
			pegaTodoList: pegaTodoList,
			pegaTechList: pegaTechList,
			pegaGuestList: pegaGuestList,
			pegaChecklist: pegaChecklist,
			pegaShopList: pegaShopList,
			pegaCronogramas: pegaCronogramas,
			pegaFornecedores: pegaFornecedores,
			pegaBalanco: pegaBalanco,
			pegaGrouper: pegaGrouper,
			daGroup: daGroup,
			getGallery: getGallery,
			getMeetings: getMeetings,
			pegaTudo: pegaTudo
		};
		return accessBack;

		function pegaUsuario(key){
			return userPath.child(key);
		}

		function pegaEvento(key) {
			return eventPath.child(key);
		}

		function pegaEventList(chave){
			return eventPath.child(chave);
		}		

		function pegaTechList(key){
			return techPath.child(key);
		}

		function pegaTodoList(key){
			return todoPath.child(key);
		}

		function pegaGuestList(key) {
			// body...
			return guestPath.child(key);
		}

		function pegaChecklist(key){
			return checkPath.child(key);
		};

		function pegaShopList(key){
			return shopPath.child(key);
		}

		function pegaCronogramas(key){
			return cronoLists.child(key);
		};

		function pegaFornecedores(key) {
			return workLists.child(key);
		}

		function pegaBalanco(key) {
			return moneyPath.child(key);
		}

		function pegaGrouper(key) {
			return groupPath.child(key);
		}

		function daGroup() {
			return groupPath;
		}

		function getGallery(key) {
			return galleryPath.child(key);
		}

		function getMeetings(key) {
			return meetPath.child(key);
		}
		
		function pegaTudo(){
			return nakedPath;
		};		
		
	}; // fim da função principal
})(); // fim do arquivo JS
