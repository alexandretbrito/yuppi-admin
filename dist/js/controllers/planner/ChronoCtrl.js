(function() {
'use_strict';

angular.module('app').controller('ChronoCtrl', ChronoCtrl);

function ChronoCtrl($scope, $rootScope, planFactory, accessFactory, Utils, $filter, ipCookie){

    console.log($rootScope.chooseEvt);
    $scope.editedID = $rootScope.chooseEvt;
    $scope.varUser = ipCookie('sessao');
    var chronObj = {} 

    $scope.listCheck = [];

    $scope.starter = function(){
      console.log("starter");
      var trans = accessFactory.pegaCronogramas($scope.varUser.uid);
      var traste = trans.child($scope.editedID);
      traste.on("value", function(snapshot){
        chronObj = snapshot.val();
        console.log(chronObj)
        chron_passodois()
      });      
    }


    function chron_passodois() {
      // body...
      if($rootScope.asInvited){
        if(chronObj){
            var bjec = {};
            bjec = chronObj;
            angular.forEach(bjec, function(shops, key){

                $scope.listCheck[key] = angular.copy(shops);
                //console.log(bjec)
            });
            console.log("vai")
            console.log(bjec)
            $scope.$apply();
        }else{
            console.log("segue vazio")
        }
      }else{
        if(chronObj){
            var bjec = {};
            bjec = chronObj;
            angular.forEach(bjec, function(shops, key){

                $scope.listCheck[key] = angular.copy(shops);
                //console.log(bjec)
            });
            console.log("vai")
            console.log(bjec)
            $scope.$apply();
        }else{
            console.log("segue vazio")
        }
      }
  }

        $scope.showTaskPrompt = function() {
            var newShop = {
                item: '',
                time: '14:00',
                done: false
            };  

              $scope.newShop = newShop;
              var chop = $scope.newShop.time;
              var arr = chop.split(':');
              var hr = arr[0];
              var mn = arr[1];
              var saveData = new Date(0);
              saveData.setHours(hr);
              saveData.setMinutes(mn);
              $scope.itemTime = {
                  value: saveData
              };
        };

        $scope.saveTask = function(indice) {
          if($scope.newShop.item != ''){
              console.log("gravou")
              var datafinal = $filter('date')($scope.itemTime.value, 'HH:mm');
              $scope.newShop.time = datafinal;
              $scope.listCheck.push($scope.newShop);
              //$scope.closeModal(indice);
              $scope.insertIntoObj();
          }else{
              Utils.alertshow("Erro", "Indique o nome do item")
          }
          $('#newCrono').modal('hide')
        };

        $scope.cancelTask = function(indice) {
            //$scope.closeModal(indice);
        };

///////////////////////////////////////////////////////////////////////////////////

$scope.eventTime = {
        callback: function (val) {      //Mandatory
            console.log ("este é "+val);
            $scope.itemTime = val;
            $scope.horaok = true
        },
        //inputTime: 50400,   //Optional
        format: 24,         //Optional
        step: 5,           //Optional
        setLabel: 'Ok'    //Optional
      };

         $scope.normHour = function(){
         ionicTimePicker.openTimePicker($scope.eventTime);
       };

        $scope.acertaZero = function(minuts){
            if(minuts !== 0){
                return minuts
            }else{
                console.log("ops");
                return "00"
            }
        };   

        $scope.horeador = function(tictac){
            var selectedTime = new Date(tictac * 1000);
            console.log('Selected epoch is : ', tictac, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
            var toe = selectedTime.getUTCHours()+":"+$scope.acertaZero(selectedTime.getUTCMinutes());
            if(toe !== '0:00' || $scope.horaok === true){
                return toe;
            }else{
                return "";
            }
        };

///////////////////////////////////////////////////////////////////////////////////

    $scope.explainMe = function(state){
        state.done = true;
        if($rootScope.asInvited){
          $localStorage.colabs[$scope.theEvent].chron = $scope.listCheck;
        }else{
          $localStorage.eventos[$scope.theEvent].chron = $scope.listCheck;
        }
        $scope.insertIntoObj();
    }

    $scope.removeMe = function(itim){
        console.log(itim.item)
        //$scope.listCheck.splice(indi, 1)
        var aquele = $scope.listCheck.indexOf(itim)
        console.log(aquele)
        $scope.listCheck.splice(aquele, 1)
        $scope.insertIntoObj();
    }

    $scope.delEvento = function(){
       // A confirm dialog
       var confirmPopup = $ionicPopup.confirm({
         title: 'Deletar',
         template: 'Tem certeza que quer deletar?',
         cssClass: 'my-popup',
         buttons: [
           { text: 'Não',
              type: 'button-assertive myred',
            },
         {
           text: 'Sim',
           type: 'button-positive-900',
           onTap: function() { 
             console.log('You are sure');
           $scope.removefromUpdate(2)        
           }
         }
        ]
       })

       confirmPopup.then(function(res) {
        console.log(res)
         if(res) {

         } else {
           console.log('You are not sure');
         }
       });
    }; 

    $scope.removefromUpdate = function(indice){
        //$scope.listCheck.splice(indi, 1)
        $scope.listCheck.splice($scope.indiceMod, 1)
        //$scope.closeModal(indice);
        $scope.insertIntoObj();
    }    

    $scope.doneClicked = function(item){
        var index = $scope.listCheck.indexOf(item)
        console.log(index);
        console.log(item);
        if($scope.listCheck[index].done === false){
            $scope.listCheck[index].done = true;
        }else{$scope.listCheck[index].done = false}
        $scope.insertIntoObj();
    };

     $scope.updateClicked = function(){
        if($scope.modifItem.done === false){
            $scope.modifItem.done = true;
        }else{$scope.modifItem.done = false}
        //$scope.insertIntoObj();
    };   

    $scope.grave_me = function(objeto) {
        // body...
        planFactory.insertCrono($scope.varUser.uid, $scope.editedID, objeto)
    }   


    $scope.insertIntoObj = function(){
        // insere o array dentro de um Objeto
        var bje = {}
        angular.forEach($scope.listCheck, function(shopes, keyo){
            bje[keyo] = $scope.listCheck[keyo];
            bje = angular.copy(bje);
            console.log(bje)
        });
        $scope.grave_me(bje);        
    };


    $scope.editTask = function(obito){
        console.log(obito)
        var index = $scope.listCheck.indexOf(obito)
        console.log(index);

        $scope.indiceMod = index
        $scope.modifItem = {};
        $scope.modifItem = obito;
        $scope.savedItem = obito.item
        $scope.savedStatus = obito.done;
        $scope.backupItem = {};
        $scope.backupItem = $scope.modifItem;
              if($scope.modifItem.time == 0){
                  var datainicial = new Date(0);
                  $scope.itemTime = {
                   value: datainicial
                  };  
              }else{
                  var chop = $scope.modifItem.time;
                  var arr = chop.split(':');
                  var hr = arr[0];
                  var mn = arr[1];
                  var datainiciada = new Date(0);
                  datainiciada.setHours(hr);
                  datainiciada.setMinutes(mn);
                  $scope.itemTime = {
                      value: datainiciada
                  };          
              }; 
        console.log("entrou")
        console.log($scope.modifItem)        
    };  

    $scope.updateTask = function() {
        console.log("saiu")
        var datafinal = $filter('date')($scope.itemTime.value, 'HH:mm');
        $scope.modifItem.time = datafinal;
        console.log($scope.modifItem)
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        //$scope.closeModal(indice);
        $scope.insertIntoObj();
        $('#modCrono').modal('hide')
    };

    $scope.notUpdateTask = function(item){
        console.log("saiu sem salvar");
        $scope.modifItem.item = $scope.savedItem;
        $scope.modifItem.done = $scope.savedStatus;
        $scope.listCheck[$scope.indiceMod] = $scope.modifItem;
        delete $scope.backupItem;
        delete $scope.modifItem;
      }

}; // fim da função
})(); // fim do documento