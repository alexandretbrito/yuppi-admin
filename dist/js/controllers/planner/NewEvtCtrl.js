(function() {
'use_strict';

angular.module('app').controller('NewEvtCtrl', NewEvtCtrl);

function NewEvtCtrl($scope, $rootScope, $sessionStorage, Utils, $location, planFactory, accessFactory, $filter, $timeout, ipCookie){


	//$scope.$on("$ionicView.enter", function(event, data){
	   $scope.evtPreCad = true;
	   $scope.naocasorio = true;
	   $scope.mySelect = "Clique para escolher";
		$scope.evtType = "";
		$scope.newEvt = {};
		$scope.allEvents = {};
 		$scope.varUser = ipCookie('sessao');
	//});

	    //$scope.$on("$ionicView.leave", function(event){
	    $scope.$on('$destroy', function(){
			$scope.dataDoEvento = null;
			$scope.horaDoEvento = null;
			$scope.eventer = {
				otherEvtType: ""
			};	
			delete $scope.itemTime.value;
			delete $scope.churchTime.value;
			delete $scope.partyTime.value;
			$scope.itemDate = new Date();
			$scope.evtType = "";
			$scope.NewEvtCtrl = null;
			$scope.allEvents = null;
	        console.log("saí daqui"); 
	    });	



 $scope.changeEventType = function(sel){
  	console.log(sel);
  		$scope.evtType = sel; 
  	if(sel == "Outro"){
  		$scope.evtPreCad = false;
  		$scope.naocasorio = true;	
  	}
  	 else if(sel == "Casamento"){
  		$scope.naocasorio = false;
  		$scope.evtPreCad = true;		
  	}else{
  		$scope.naocasorio = true;
  		$scope.evtPreCad = true;
  	}
  }


  $scope.starter = function(){
  	$rootScope.onEvent = false;
  }

/////////////////////////////////////////////////////////
//					Objetos de Tempo
/////////////////////////////////////////////////////////

	var timeSeed = "18:00";

	var arr = timeSeed.split(':');
	var hr = arr[0];
	var mn = arr[1];
	var saveData = new Date(0);
	saveData.setHours(hr);
	saveData.setMinutes(mn);

	var saveDay = new Date();

	$scope.itemTime = {
    	value: saveData
    	}; 
	$scope.churchTime = {
    	value: saveData
    }; 
	$scope.partyTime = {
    	value: saveData
   	}; 

    $scope.itemDate = {
     	value: saveDay
   	}; 

//////////////////////////////////////////////////////////

function engMonth(mes){
var abrev = "";
if(mes === "01"){
	abrev = "Jan"; 
}else if(mes === "02"){
	abrev = "Feb";	
}else if(mes === "03"){
	abrev = "Mar";	
}else if(mes === "04"){
	abrev = "Apr";	
}else if(mes === "05"){
	abrev = "May";	
}else if(mes === "06"){
	abrev = "Jun";	
}else if(mes === "07"){
	abrev = "Jul";	
}else if(mes === "08"){
	abrev = "Aug";	
}else if(mes === "09"){
	abrev = "Sep";	
}else if(mes === "10"){
	abrev = "Oct";	
}else if(mes === "11"){
	abrev = "Nov";	
}else if(mes === "12"){
	abrev = "Dec";	
}

return abrev;

}



var ID = function () {
  return 'APP' + Math.random().toString(36).substr(2, 9);
};


$scope.vambora = function(){
		$location.path('/eventos/ver_eventos');
}

$scope.marcador = function(marcacao){
	var codice = marcacao;

	if(codice.cont01Nome != undefined && codice.cont01Phone == undefined || codice.cont01Nome == undefined && codice.cont01Phone != undefined){
		Utils.alertshow("Erro", "Primeiro contato extra incompleto")
	}
	else if(codice.cont02Nome != undefined && codice.cont02Phone == undefined || codice.cont02Nome == undefined && codice.cont02Phone != undefined){
		Utils.alertshow("Erro", "Segundo contato extra incompleto")
	}
	else if(codice.cont03Nome != undefined && codice.cont03Phone == undefined || codice.cont03Nome == undefined && codice.cont03Phone != undefined){
		Utils.alertshow("Erro", "Terceiro contato extra incompleto")
	}
	else if(codice.cont04Nome != undefined && codice.cont04Phone == undefined || codice.cont04Nome == undefined && codice.cont04Phone != undefined){
		Utils.alertshow("Erro", "Quarto contato extra incompleto")
	}
	else{
		$scope.submarcador(codice)
	}

}

$scope.submarcador = function(marcacao2){	

	var horaevento = $filter('date')($scope.itemTime.value, 'HH:mm');
	console.log(horaevento);
	var dataEvento = $filter('date')($scope.itemDate.value, 'dd/MM/yyyy');
	console.log(dataEvento);

	var marker = marcacao2;
	var refined_evt = {};
	var shaca = dataEvento;
	var utcMes = shaca.split("/");
	var diaCal = utcMes[0];
	var anoCal = utcMes[2];
	var mesCal =  utcMes[1];
	var calId = diaCal+"-"+engMonth(mesCal)+"-"+anoCal;


	refined_evt.evtHour = horaevento;
	refined_evt.evtDay = dataEvento;
	refined_evt.calId = calId;

	//marker.ID = ID();
	refined_evt.ID = ID();	

	if($scope.evtType == "Casamento"){
		var horaCasorio = $filter('date')($scope.churchTime.value, 'HH:mm');
		console.log("Casa às "+horaCasorio);
		//marker.horaCasorio = horaCasorio;
		refined_evt.horaCasorio = horaCasorio;
		var horaFesta = $filter('date')($scope.partyTime.value, 'HH:mm');
		console.log("Festa às "+horaCasorio);		
		//marker.horaFesta = horaFesta;
		refined_evt.horaFesta = horaFesta;
	}



	if($scope.evtType !== "Outro"){
		//marker.evtType = $scope.evtType;
		refined_evt.evtType = $scope.evtType;
	}
	if($scope.evtType == "Outro"){
		//marker.evtType = $scope.otherEvtType;
		console.log("É outro. Talvez seja "+marker.otherEvtType)
		refined_evt.evtType = marker.evtType;
	}
	if($scope.evtType == "Clique para escolher"){
		//marker.evtType = "";
		refined_evt.evtType = "";
	}

	//passando os outros para refined_evt

	refined_evt.client_name = marker.client_name;
	refined_evt.client_phone = marker.client_phone;
	refined_evt.name = marker.name;
	refined_evt.place = marker.place;
	if(marker.client_mail){
		refined_evt.client_mail = marker.client_mail;
	}
	//Co-resonsável
	if(marker.client2_name){
		refined_evt.client2_name = marker.client2_name;
	}
	if(marker.client2_phone){
		refined_evt.client2_phone = marker.client2_phone;
	}
	if(marker.client2_mail){
		refined_evt.client2_mail = marker.client2_mail;
	}		

	//Contato 1
	if(marker.cont01Nome){
		refined_evt.cont01Nome = marker.cont01Nome;
	}
	if(marker.cont01Phone){
		refined_evt.cont01Phone = marker.cont01Phone;
	}

	//Contato 2
	if(marker.cont02Nome){
		refined_evt.cont02Nome = marker.cont02Nome;
	}
	if(marker.cont02Phone){
		refined_evt.cont02Phone = marker.cont02Phone;
	}

	//Contato 3
	if(marker.cont03Nome){
		refined_evt.cont03Nome = marker.cont03Nome;
	}
	if(marker.cont03Phone){
		refined_evt.cont03Phone = marker.cont03Phone;
	}

	//Contato 4
	if(marker.cont04Nome){
		refined_evt.cont04Nome = marker.cont04Nome;
	}
	if(marker.cont04Phone){
		refined_evt.cont04Phone = marker.cont04Phone;
	}


	refined_evt = angular.copy(refined_evt)

	console.log(marker)
	console.log("////////////////")
	console.log(refined_evt)

	//if($localStorage.eventos){
			acabe_de_gravar(refined_evt);	
	//}else{
	//	$localStorage.eventos = {};
	//	acabe_de_gravar(refined_evt);
	//}
	//evalEvents();
	//Utils.show();

};


	

	function acabe_de_gravar(petardo) {
		// body...
		var objet = {};
		objet = petardo;
		console.log(objet)
		//$localStorage.eventos[objet.ID] = objet;

		//console.log($localStorage.eventos)		

		//if($scope.connec){
			console.log("vou gravar na internet")
			planFactory.insertEvt($scope.varUser.uid, objet.ID, objet)
		//}else{
		//	console.log("não vou gravar na internet")
		//	ionicToast.show('Não há conexão com a internet. Os dados serão estocados somente no aparelho até o próximo salvamento', 'middle', false, 3000);
		//}

		$timeout(function(){
			//Utils.hide();
			$location.path('/dashboard');			
		}, 1000);


	}	

	$scope.editEvento = function(evt){
		$rootScope.editableEvt = evt;
	}


}; // fim da função
})(); // fim do documento