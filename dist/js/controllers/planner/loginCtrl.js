(function() {
'use_strict';

angular.module('app').controller('loginCtrl', loginCtrl);

function loginCtrl($scope, $rootScope, $location, $stateParams, $timeout, $firebaseObject, FireAuth, Utils, $sessionStorage){
    // Form data for the login modal
    $scope.user = {
      email: '',
      password: ''
    };

    $rootScope.isLogged = false;


    // handle event
    console.log("LogCtrl começa");
    //$scope.$parent.hideHeader();

    $scope.comparate = function(){
      /*
      if($localStorage.usuario){
        if($localStorage.usuario.fbSign){
          console.log("não segue");
          $localStorage.$reset();
        }else{
          console.log("segue");
          if($localStorage.eventos){
            console.log("eventos estocados");
            console.log("Purgar elementos");
            var bje = {}
            bje = $localStorage.eventos;
            angular.forEach(bje, function(shopes, keyo){
              if(!shopes.ID){
                delete bje[keyo]
              }
            });
            $localStorage.eventos = bje;
            console.log($localStorage.eventos)
          }

          
        }
      }*/
      //$location.path("app/calendario");
    }

    $scope.registro_simples = function(usuario){
      console.log(usuario)
      if(usuario.email != undefined && usuario.password  != undefined){
          $scope.user.email = usuario.email;
          $scope.user.password = usuario.password;
          FireAuth.register($scope.user);
      }else{
          Utils.alertshow("Erro", "Insira os dados corretamente")
      }
    };

    $scope.login_simples = function(usuario){
      $scope.user.email = usuario.email;
      $scope.password = usuario.password;
      FireAuth.login(usuario);
    };

  $scope.faceLogin = function(){
    console.log("faceLogin")
    FireAuth.signFB();
  };

   $scope.directUser = function(){
    $location.path("/dashboard");
   };

  $rootScope.$on('setEvents', function(ev){
      $location.path("/dashboard");
  }) 

  $scope.comparate();

}; // fim da função
})(); // fim do documento