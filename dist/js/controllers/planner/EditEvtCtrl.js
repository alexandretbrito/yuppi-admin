(function() {
'use_strict';

angular.module('app').controller('EditEvtCtrl', EditEvtCtrl);

function EditEvtCtrl($scope, $rootScope, $sessionStorage, Utils, $location, planFactory, accessFactory, $filter, $timeout, ipCookie){
//   $scope.$parent.showHeader();
//    $scope.isExpanded = false;
//    $scope.$parent.setExpanded(false);
//    $scope.$parent.setHeaderFab(false);

	//$scope.$on("$ionicView.enter", function(event, data){
		$scope.evtType = "";
		$scope.editedEvt = {};
		$scope.allEvents = {};
		$scope.editedEvt = {};
		$scope.editedID = $rootScope.chooseEvt;
		$scope.varUser = ipCookie('sessao');
		console.log($scope.varUser)

		showContent();

		function starter() {
		var delEvts = accessFactory.pegaEvento($scope.varUser.uid);
		var delEvts2 = delEvts.child($scope.editedID);
			delEvts2.on("value", function(snapshot){
				$scope.editedEvt = snapshot.val();

				$scope.evtType = $scope.editedEvt.evtType
			   $scope.evtPreCad = true;
			   if($scope.editedEvt.evtType == "Casamento"){
					$scope.naocasorio = false;
			   }else{
					$scope.naocasorio = true;
			   }
			   $scope.mySelect = $scope.editedEvt.evtType;

				console.log($scope.editedEvt.evtHour +" em cima")
				spreadTime();
			});

		};


	$scope.$on('$destroy', function(){
		console.log("saindo dda edição do evento")
		//$scope.dataDoEvento = null;
		//$scope.horaDoEvento = null;
		//$scope.otherEvtType = "";
		//$scope.evtType = "";
		//$scope.allEvents = null;
        console.log("saí daqui"); 
	});

 $scope.changeEventType = function(sel){
  	console.log(sel);
  		$scope.evtType = sel; 
  	if(sel == "Outro"){
  		$scope.evtPreCad = false;
  		$scope.naocasorio = true;	
  	}
  	 else if(sel == "Casamento"){
  		$scope.naocasorio = false;
  		$scope.evtPreCad = true;		
  	}else{
  		$scope.naocasorio = true;
  		$scope.evtPreCad = true;
  	}
  }



/////////////////////////////////////////////////////////
//					Objetos de Tempo
/////////////////////////////////////////////////////////

	function spreadTime(){
		// body...
	//horado evento
	var timeSeed = $scope.editedEvt.evtHour;
	var arr = timeSeed.split(':');
	var hr = arr[0];
	var mn = arr[1];
	var saveData = new Date(0);
	saveData.setHours(hr);
	saveData.setMinutes(mn);
	$scope.itemTime = {
    	value: saveData
	};
	console.log($scope.itemTime.value +" de horas") 

	var diaD = $scope.editedEvt.evtDay;
	var diaArr = diaD.split('/');
	var finalCountDown = new Date(diaArr[2], diaArr[1], diaArr[0]);
    $scope.itemDate = {
     	value: finalCountDown
   	}; 


   	// dia e hora do casamento
   	if($scope.editedEvt.evtType === "Casamento"){

	var timeSeed2 = $scope.editedEvt.horaCasorio;
		var arr2 = timeSeed2.split(':');
		var hr2 = arr2[0];
		var mn2 = arr2[1];
		var saveData2 = new Date(0);
		saveData2.setHours(hr2);
		saveData2.setMinutes(mn2);
		$scope.churchTime = {
	    	value: saveData2
	   	}; 

	var timeSeed3 = $scope.editedEvt.horaFesta;
		var arr3 = timeSeed3.split(':');
		var hr3 = arr3[0];
		var mn3 = arr3[1];
		var saveData3 = new Date(0);
		saveData3.setHours(hr3);
		saveData3.setMinutes(mn3);

		$scope.partyTime = {
	    	value: saveData3
	   	}; 
	   }
	   				console.log($scope.editedEvt)
	}
//////////////////////////////////////////////////////////

function engMonth(mes){
var abrev = "";
if(mes === "01"){
	abrev = "Jan"; 
}else if(mes === "02"){
	abrev = "Feb";	
}else if(mes === "03"){
	abrev = "Mar";	
}else if(mes === "04"){
	abrev = "Apr";	
}else if(mes === "05"){
	abrev = "May";	
}else if(mes === "06"){
	abrev = "Jun";	
}else if(mes === "07"){
	abrev = "Jul";	
}else if(mes === "08"){
	abrev = "Aug";	
}else if(mes === "09"){
	abrev = "Sep";	
}else if(mes === "10"){
	abrev = "Oct";	
}else if(mes === "11"){
	abrev = "Nov";	
}else if(mes === "12"){
	abrev = "Dec";	
}

return abrev;

}


$scope.vambora = function(){
		$location.path('/eventos/ver_eventos');
}

$scope.marcador = function(marcacao){

	var horaevento = $filter('date')($scope.itemTime.value, 'HH:mm');
	console.log(horaevento);
	var dataEvento = $filter('date')($scope.itemDate.value, 'dd/MM/yyyy');
	console.log(dataEvento);

	var marker = marcacao;

	var shaca = dataEvento;

	var utcMes = shaca.split("/");
	var diaCal = utcMes[0];
	var anoCal = utcMes[2];
	var mesCal =  utcMes[1];
	var calId = diaCal+"-"+engMonth(mesCal)+"-"+anoCal;

	marker.evtHour = horaevento;
	marker.evtDay = dataEvento;
	marker.calId = calId;

	console.log($scope.evtType);
	if($scope.editedEvt.evtType != $scope.evtType){
		marker.evtType = $scope.evtType;
		console.log("troquei")
	}

	marker.evtType


	if($scope.evtType == "Casamento"){
		var horaCasorio = $filter('date')($scope.churchTime.value, 'HH:mm');
		console.log("Casa às "+horaCasorio);
		marker.horaCasorio = horaCasorio;
		var horaFesta = $filter('date')($scope.partyTime.value, 'HH:mm');
		console.log("Festa às "+horaFesta);		
		marker.horaFesta = horaFesta;
	}

	marker = angular.copy(marker)

	acabe_de_gravar(marker);
};


	function acabe_de_gravar(petardo) {
		// body...
		var objet = {};
		objet = petardo;
		console.log(objet)
		//$localStorage.eventos[objet.ID] = objet;

		//console.log($localStorage.eventos)		

		//if($scope.connec){
			console.log("vou gravar na internet")
			planFactory.insertEvt($scope.varUser.uid, objet.ID, objet)
		//}else{
		//	console.log("não vou gravar na internet")
		//	ionicToast.show('Não há conexão com a internet. Os dados serão estocados somente no aparelho até o próximo salvamento', 'middle', false, 3000);
		//}

		$timeout(function(){
			$location.path('/eventos/ver_eventos');			
		}, 600);


	}	 

   function showContent() {
   	// body...
	   $timeout(function(){
			$scope.timePaster = true;
			console.log("demorou...")
			starter();
	   }, 0);
   }


}; // fim da função
})(); // fim do documento