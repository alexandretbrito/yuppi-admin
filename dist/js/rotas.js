angular
.module('app')
.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {
  $stateProvider
  //eventos
  .state('app.eventos', {
    url: "/eventos",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Eventos'
    }
  })
  .state('app.eventos.ver_eventos', {
    url: '/ver_eventos',
    templateUrl: 'views/planner/ver_eventos.html',
    controller: 'EvtsCtrl',
    authenticate: true,
    ncyBreadcrumb: {
      label: 'Lista'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/EvtsCtrl.js']
        });
      }]
    }
  })
  .state('app.eventos.novo_evento', {
    url: '/novo_evento',
    templateUrl: 'src/views/planner/novo_evento.html',
    controller: 'NewEvtCtrl',
    authenticate: true,
    ncyBreadcrumb: {
      label: 'Novo Evento'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/NewEvtCtrl.js']
        });
      }]
    }
  })
  .state('app.eventos.edita_evento', {
    url: '/editar_evento',
    templateUrl: 'views/planner/edita_evento.html',
    controller: 'EditEvtCtrl',
    authenticate: true,
    ncyBreadcrumb: {
      label: 'Editar Evento'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/EditEvtCtrl.js']
        });
      }]
    }
  })
  .state('app.eventos.eventounico', {
    url: '/eventounico',
    templateUrl: 'views/planner/evento_unico.html',
    controller: 'TheEvtCtrl',
    authenticate: true,
    ncyBreadcrumb: {
      label: 'Evento'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/TheEvtCtrl.js']
        });
      }]
    }
  })
  //to do
  .state('app.todos', {
    url: "/todos",
    abstract: true,
    authenticate: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Bloco de Notas'
    },
  })
  .state('app.todos.ver_todos', {
    url: '/ver_todos',
    templateUrl: 'views/planner/ver_todos.html',
    authenticate: true,
    controller: 'ToDoListCtrl',
    ncyBreadcrumb: {
      label: 'Anotações'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/ToDoListCtrl.js']
        });
      }]
    }
  })
  //visita técnica
  .state('app.visita-tecnica', {
    url: "/visita-tecnica",
    abstract: true,
    authenticate: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Bloco de Notas'
    },
  })
  .state('app.visita-tecnica.ver-visita-tecnica', {
    url: '/ver_visita-tecnica',
    templateUrl: 'views/planner/ver-visita-tecnica.html',
    authenticate: true,
    controller: 'visitaTecnicaCtrl',
    ncyBreadcrumb: {
      label: 'Anotações'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/visitaTecnicaCtrl.js']
        });
      }]
    }
  })  
  //compras
  .state('app.compras', {
    url: "/compras",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Compras'
    }
  })
  .state('app.compras.ver_compras', {
    url: '/compras',
    templateUrl: 'views/planner/ver_compras.html',
    authenticate: true,
    controller:'ShopListCtrl',
    ncyBreadcrumb: {
      label: 'Lista'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/ShopListCtrl.js']
        });
      }]
    }
  })
  //checklist
  .state('app.checklist', {
    url: "/checklist",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Checklist'
    }
  })
   .state('app.checklist.ver_checklist', {
    url: '/checklist',
    templateUrl: 'views/planner/ver_checklist.html',
    authenticate: true,
    controller: 'ChecklistCtrl',
    ncyBreadcrumb: {
      label: 'Lista'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/ChecklistCtrl.js']
        });
      }]
    }
  })
  //cronograma
  .state('app.cronograma', {
    url: "/cronograma",
    abstract: true,
    authenticate: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Cronograma'
    }
  })
  .state('app.cronograma.cronograma_evento', {
    url: "/cronograma_evento",
    templateUrl: 'views/planner/cronograma.html',
    authenticate: true,
    controller: 'ChronoCtrl',
    ncyBreadcrumb: {
      label: 'Cronograma de Evento'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/ChronoCtrl.js']
        });
      }]
    }
  })
  .state('app.cronograma.cronograma_infantil', {
    url: "/cronograma_infantil",
    templateUrl: 'views/planner/cronograma_kid.html',
    authenticate: true,
    controller: 'ChronoKidCtrl',
    ncyBreadcrumb: {
      label: 'Cronograma de Evento'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/ChronoKidCtrl.js']
        });
      }]
    }
  })
  .state('app.cronograma.cronograma_casamento', {
    url: "/cronograma_casamento",
    templateUrl: 'views/planner/cronograma_casorio.html',
    authenticate: true,
    controller: 'ChronoWedCtrl',
    ncyBreadcrumb: {
      label: 'Cronograma de Evento'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/ChronoWedCtrl.js']
        });
      }]
    }
  })
  //receitas_despesas
  .state('app.receitas_despesas', {
    url: "/receitas_despesas",
    abstract: true,
    authenticate: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Receitas e Despesas'
    }
  })
  .state('app.receitas_despesas.ver_receitas_despesas', {
    url: '/ver_receitas_despesas',
    templateUrl: 'views/planner/ver_receitas_despesas.html',
    authenticate: true,
    controller: 'BillsCtrl',
    ncyBreadcrumb: {
      label: 'Receitas e Despesas'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/BillsCtrl.js']
        });
      }]
    }
  })
  //Fornecedores
  .state('app.fornecedores', {
    url: "/fornecedores",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Fornecedores'
    }
  })
  .state('app.fornecedores.ver_fornecedores', {
    url: '/ver_fornecedores',
    templateUrl: 'views/planner/ver_fornecedores.html',
    controller: 'SuppliersCtrl',
    authenticate: true,
    ncyBreadcrumb: {
      label: 'Lista'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/SuppliersCtrl.js']
        });
      }]
    }
  })
  //rsvp
  .state('app.rsvp', {
    url: "/rsvp",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'RSVP'
    }
  })
  .state('app.rsvp.ver_confirmacao', {
    url: '/ver_confirmacao',
    templateUrl: 'views/planner/ver_confirmacao.html',
    controller: 'rsvpCtrl',
    authenticate: true,
    ncyBreadcrumb: {
      label: 'Confirmação de convidados'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/rsvpCtrl.js']
        });
      }]
    }
  })
  //reuniões
  .state('app.reunioes', {
    url: "/reunioes",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'REUNIÕES'
    }
  })
  .state('app.reunioes.ver_reunioes', {
    url: '/ver_reunioes',
    templateUrl: 'views/planner/ver_reunioes.html',
    controller: 'reunioesCtrl',
    authenticate: true,
    ncyBreadcrumb: {
      label: 'Reuniões sobre o evento'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/reunioesCtrl.js']
        });
      }]
    }
  })
  //grupos
  .state('app.grupo', {
    url: "/grupo",
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'GRUPO'
    }
  })
  .state('app.grupo.grupo', {
    url: '/grupo',
    templateUrl: 'views/planner/grupo.html',
    controller: 'grupoCtrl',
    authenticate: true,
    ncyBreadcrumb: {
      label: 'Gerenciar grupo'
    },
    resolve: {
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load({
          files: ['js/controllers/planner/reunioesCtrl.js']
        });
      }]
    }
  })
}]);