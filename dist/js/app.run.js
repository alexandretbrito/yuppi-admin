(function(){
	'use strict';
	angular.module('app')
	.run(authRun);

	function authRun($rootScope, $state, $sessionStorage, ipCookie) {
		// body...
      console.log(ipCookie('sessao'))
      $rootScope.$on("$stateChangeStart", function(event, curr, prev){
      // Check .state config to determine if authentication is needed 
      // and if so check if User is authenticated
      console.log("aqui vai o run...");
      console.log(curr.authenticate);
      var biscoito = {}
      if(ipCookie('sessao')){
        biscoito = ipCookie('sessao');
      }
      if (curr.authenticate && !biscoito.logado || curr.authenticate && biscoito.logado === undefined) {
        // User isn’t authenticated
        console.log("precisa logar")
        $state.transitionTo("login");
        event.preventDefault(); 
      }else{
      	console.log("pode avançar");
        if($state.name !== "login"){
          $rootScope.$isSigned = true;
        }
      }
    });
	}
  })();